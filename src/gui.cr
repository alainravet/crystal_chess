PV_TABLE_SIZE = (2**20).to_i
DEBUG_MODE    = true

require "./load_all"
include Chess

board = Board.parse_ascii("
  K.......
  ........
  ..P.....
  ........
  ..B.....
  ........
  .p......
  k.......
  w - - 0 7
  ")
Chess::Debug.print_board board
puts "-" * 88

while true
  puts "history = #{board.history[0..board.his_ply].compact.map(&.move as Move).map(&.to_s)}"
  puts "his_ply = #{board.his_ply}"
  print "Please enter a move:\n> "

  break unless  l = gets
  case (l = l.chomp)
  when "q"
    exit
  when "t"
    MoveMaker.new(board).take_move
  when "p" # TODO : refactor
    max = board.get_pv_line(1)
    printf "PvLine of #{max} Moves: "
    puts board.pv_array[0...max].map { |move|
       move ? move.to_s_compact : "nil"
    }.join(" ")

  else
    valid_moves = MoveGenerator.new(board, board.side).generate.map(&.move)
    move = Move::Parser.parse(l, valid_moves)
    if move == Move::NOMOVE
      puts "Invalid move\nMust be one of #{valid_moves.map(&.to_s_compact).sort}\n\n"

    else
      # Pretend this was the best theoretical move so we can 1°/ fill
      # the PV table and 2°/ later (with "p") retrieve and print the PV line :
      board.store_pv_move(move)

      MoveMaker.new(board).make_move move
      Chess::Debug.print_board board
      if board.repetition_detected?
        puts "repetition detected"
      end
    end
  end
end
