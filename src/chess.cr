PV_TABLE_SIZE = $test_mode ? $test_pv_table_size : (2**20).to_i
DEBUG_MODE    = $test_mode ? true : false
require "./load_all"
include Chess


# require "logger"
# $log_file = File.new(File.expand_path("debug.log"), "w")
# $log = Logger.new($log_file)
# $log.formatter = Logger::Formatter.new do |severity, datetime, progname, message, io|
#     io << message
#   end
# $log.level = Logger::DEBUG
#
#require "./chess/perft"
#Perft.test_and_print(Board.parse_fen(FEN_START_POSITION), 2 )
