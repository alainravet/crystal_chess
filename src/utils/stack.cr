module Utils
  class Stack(T)
    class EmptyStack < Exception ; end
    class CapacityError < Exception ; end

    def initialize(max_size=50)
      @store = StaticArray(T|Nil,3).new {nil}
      @next_idx = 0
      @max_size = max_size
    end

    def size
      @next_idx
    end

    def empty?
      size == 0
    end

    def full?
      size == @max_size
    end

    def values
      return [] of T if empty?
      @store.values_at(0, @next_idx-1)
    end

    def push(val : T)
      raise CapacityError.new if full?
      @store[@next_idx] = val
      @next_idx += 1
      self
    end

    def top : T|Nil
      return nil if empty?
      @store[@next_idx-1]
    end

    def pop : T|Nil
      raise EmptyStack.new if empty?
      val = top
      @next_idx -= 1
      val
    end
  end
end
