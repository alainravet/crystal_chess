module Utils
  class Pool(T)
    getter :used

    def initialize
      @used = 0
      @buckets = Array(T).new
    end

    def get_one : T
      all_buckets_used = (@buckets.size == @used)
      if all_buckets_used
        @buckets << T.new
      else
        # recycling..
      end
      elem = @buckets[@used]
      @used += 1
      elem
    end

    def give_back(elem : T)
      #   BEFORE: @used = 3
      #           A B C   d e f g
      # pool.give_back B
      #   AFTER:  @used = 2
      #           A C b d e f g
      recycled_i   = @buckets.index(elem)
      last_elem_i  = @used - 1
      raise "BUG : element not found in pool" unless recycled_i
      @buckets[recycled_i] = @buckets[last_elem_i]
      elem.clear
      @buckets[last_elem_i] = elem
      @used -= 1
    end
  end
end
