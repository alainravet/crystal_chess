require "./load_all"
include Chess

require "benchmark"
# rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ;D1 20 ;D2 400 ;D3 8902 ;D4 197281 ;D5 4865609 ;D6 119060324
b = Board.parse_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")

puts "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

def test_level(board, level, expected)
  puts "test level #{level}"
  nof_leaves = -1
  puts Benchmark.measure {
    nof_leaves = Perft.new(board).perft(level)
    puts "@nof_leaves = #{nof_leaves}"
  }
  return if nof_leaves == expected
  puts "ERROR"
  puts "got : #{nof_leaves} - expected #{expected}"
end

# test_level b, 1,          20
# test_level b, 2,         400
# test_level b, 3,       8_902
# test_level b, 4,     197_281
test_level b, 5,   4_865_609
test_level b, 6, 119_060_324

# Alains-MacBook-Pro:chess alain$ crystal build  --release src/test.cr ; ./test
# rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
# test level 1
# @nof_leaves = 20
#   0.000000   0.000000   0.000000 (  0.000685)
# test level 2
# @nof_leaves = 400
#   0.000000   0.000000   0.000000 (  0.000220)
# test level 3
# @nof_leaves = 8902
#   0.010000   0.000000   0.010000 (  0.004282)
# test level 4
# @nof_leaves = 197281
#   0.090000   0.010000   0.100000 (  0.099977)
# test level 5
# @nof_leaves = 4865609
#   2.120000   0.330000   2.450000 (  2.266249)
# test level 6
# @nof_leaves = 119060324
#   57.680000   7.650000   65.330000 (  61.189610)
