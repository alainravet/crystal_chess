require "logger"
$log_file = File.new(File.expand_path("debug.log"), "w")
$log = Logger.new($log_file)
$log.formatter = Logger::Formatter.new do |severity, datetime, progname, message, io|
    io << message
  end
$log.level = Logger::DEBUG

require "./load_all"
include Chess

require "benchmark"
# r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1 ;D1 48 ;D2 2039 ;D3 97862 ;D4 4085603 ;D5 193690690 ;D6 8031647685
b = Board.parse_fen("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1")

puts "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1"
$expected = -1
$started_at = Time.now

def test_level(board, level, expected)
$expected = expected
$log.debug "  level #{level}:"
  printf "  level #{level}:"
  $started_at = Time.now
  nof_leaves = Perft.new(board).perft(level)
  nof_milliseconds = (Time.now - $started_at).total_milliseconds
  speed  = nof_leaves / nof_milliseconds
  printf "%15s leaves found in #{(nof_milliseconds/1000).round(2)} sec.  #{speed} leaves/ms\n", [nof_leaves]
$log.debug "#{nof_leaves} leaves found in #{(nof_milliseconds/1000).round(2)} sec.  #{speed} leaves/ms"
  return if nof_leaves == expected
  puts "ERROR"
  puts "got : #{nof_leaves} - expected #{expected}"
  raise "\n\nSTOP (all is wrong from here)\n\n"
end

test_level b, 1,             48
test_level b, 2,          2_039
test_level b, 3,         97_862
test_level b, 4,      4_085_603
test_level b, 5,    193_690_690
test_level b, 6,  8_031_647_685

# Alains-MacBook-Pro:chess alain$ crystal build  --release src/test.cr ; ./test
# rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
# test level 1
# @nof_leaves = 20
#   0.000000   0.000000   0.000000 (  0.000685)
# test level 2
# @nof_leaves = 400
#   0.000000   0.000000   0.000000 (  0.000220)
# test level 3
# @nof_leaves = 8902
#   0.010000   0.000000   0.010000 (  0.004282)
# test level 4
# @nof_leaves = 197281
#   0.090000   0.010000   0.100000 (  0.099977)
# test level 5
# @nof_leaves = 4865609
#   2.120000   0.330000   2.450000 (  2.266249)
# test level 6
# @nof_leaves = 119060324
#   57.680000   7.650000   65.330000 (  61.189610)
