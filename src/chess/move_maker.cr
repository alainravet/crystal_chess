
module Chess
  class MoveMaker
    getter :board

    def initialize(@board)
    end

    def hash_PCE(sq, piece)
      board.hash_key ^= PIECE_HKEY[sq][piece]
    end
    def hash_CA
      board.hash_key ^= CASTLE_HKEY[board.castle_perms]
    end
    def hash_SIDE
      board.hash_key ^= SIDE_HKEY
    end
    def hash_EP
      board.hash_key ^= PIECE_HKEY[board.en_pas_square][EMPTY]
    end

    def clear_square(sq)
      cleared_piece = board.piece(sq)
      col   = PIECE_COLOUR[cleared_piece]

      board.hash_key ^= PIECE_HKEY[sq][cleared_piece]
      board.clear_square sq
      board.material[col] -=  PIECE_VAL[cleared_piece]

      if pawn?(cleared_piece)
        sq64 = sq64(sq)
        board.pawns[col ].clear_bit(sq64)
        board.pawns[BOTH].clear_bit(sq64)
      else
        board.big_pieces[col] -= 1  # "big" == "not-a-pawn"
        if maj?(cleared_piece)
          board.maj_pieces[col] -= 1
        else
          board.min_pieces[col] -= 1
        end
      end

      # remove the cleared_piece from the pieces list
      piece_list = board.piece_list[cleared_piece]
      board.piece_num[cleared_piece] -= 1
      last_piece_idx = board.piece_num[cleared_piece]
      sq_offset = piece_list.index(sq)
      raise "BUG : cleared_piece not found in cleared_piece list - cleared_piece = #{cleared_piece} @ square #{sq}" unless sq_offset
      piece_list[sq_offset] = piece_list[last_piece_idx]
    end

    def add_piece(sq, piece)
      col   = PIECE_COLOUR[piece]

      board.hash_key ^= PIECE_HKEY[sq][piece]
      board.set_piece sq, piece
      board.material[col] +=  PIECE_VAL[piece]

      if pawn?(piece)
        sq64 = sq64(sq)
        board.pawns[col ].set_bit(sq64)
        board.pawns[BOTH].set_bit(sq64)
      else
        board.big_pieces[col] += 1  # "big" == "not-a-pawn"
        if maj?(piece)
          board.maj_pieces[col] += 1
        else
          board.min_pieces[col] += 1
        end
      end

      next_idx = board.piece_num[piece]
      board.piece_list[piece][next_idx] = sq
      board.piece_num[piece] += 1
    end

    def move_piece(from, to)
      piece = board.piece(from)
      col   = PIECE_COLOUR[piece]

      board.clear_square from
      board.set_piece to,   piece

      board.hash_key ^= PIECE_HKEY[from][piece]
      board.hash_key ^= PIECE_HKEY[to  ][piece]

      if pawn?(piece)
        sq64_from = sq64(from)
        sq64_to   = sq64(to)

        board.pawns[col ].clear_bit(sq64_from)
        board.pawns[BOTH].clear_bit(sq64_from)
        board.pawns[col ].set_bit(sq64_to)
        board.pawns[BOTH].set_bit(sq64_to)
      end

      piece_list = board.piece_list[piece]
      piece_list.each_with_index do |sq, offset|
        if sq == from
          piece_list[offset] = to
          break
        end
      end
    end

    CASTEL_PERM = {
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 13, 15, 15, 15, 12, 15, 15, 14, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15,  7, 15, 15, 15,  3, 15, 15, 11, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15
    };

    def make_move(move)
      from = move.from
      to   = move.to
      moved_piece = board.piece(from)
      side  = board.side

      validate_make_move_param(move, from, to, moved_piece, side) if DEBUG_MODE


      u = Undo.new(move, board.castle_perms, board.en_pas_square, board.fifty_move, board.hash_key)
      board.history[board.his_ply] = u
      board.his_ply += 1
      board.ply     += 1

      if move.en_passant?
        (side == WHITE) ?
          clear_square(to-10) :
          clear_square(to+10)

      elsif move.castling?
        case to
          when C1 then move_piece(A1, D1)
          when C8 then move_piece(A8, D8)
          when G1 then move_piece(H1, F1)
          when G8 then move_piece(H8, F8)
        end
      end

      hash_EP if board.en_pas_square != NO_SQ
      hash_CA

      board.castle_perms &= CASTEL_PERM[from]
      board.castle_perms &= CASTEL_PERM[to]
      hash_CA
      board.en_pas_square = NO_SQ;

      board.fifty_move += 1 unless move.en_passant? # TODO : TIV : not in the C-code

      if move.captured_piece != EMPTY
        clear_square to
        board.fifty_move = 0
      end

      if pawn?(moved_piece)
        board.fifty_move = 0
        if move.pawn_start?
          if side == WHITE
            board.en_pas_square = from+10
          else
            board.en_pas_square = from-10
          end
          hash_EP
        end
      end

      move_piece from, to

      promoted_piece = move.promoted_piece
      if promoted_piece != EMPTY
        clear_square(to)
        add_piece(to, promoted_piece)
      end

      if king?(moved_piece)
        board.king_squares[side] = to
      end

      board.side ^= 1
      hash_SIDE

      other_side = side ^ 1
      king_sq = board.king_squares[side]

      if AttackDetector.square_attacked_by?(board, king_sq, other_side)
        take_move
        return false
      end
      true
    end

    def take_move
      board.ply     -= 1
      board.his_ply -= 1
      undo = board.history[board.his_ply]
      if undo.nil?
        puts "board.history[#{board.his_ply}] = #{board.history[board.his_ply]}"
        raise "cannot undo nil"
      end

      move = undo.move
      from  = move.from
      to    = move.to

      hash_EP if board.en_pas_square != NO_SQ
      hash_CA

      board.castle_perms = undo.castle_perms
      board.fifty_move = undo.fifty_move
      board.en_pas_square = undo.en_pas_square

      hash_EP if board.en_pas_square != NO_SQ
      hash_CA

      board.side ^= 1
      hash_SIDE

      if move.en_passant?
        if board.side == WHITE
          add_piece to-10, BP
        else
          add_piece to+10, WP
        end
      elsif move.castling?
        case to
          when C1 then move_piece(D1, A1)
          when C8 then move_piece(D8, A8)
          when G1 then move_piece(F1, H1)
          when G8 then move_piece(F8, H8)
        end
      end

      move_piece(to, from)

      if king?(board.piece(from))
        board.king_squares[board.side] = from
      end

      captured_piece = move.captured_piece
      if captured_piece != EMPTY
        add_piece to, captured_piece
      end

      promoted_piece = move.promoted_piece
      if promoted_piece != EMPTY
        clear_square from
        pawn = PIECE_COLOUR[promoted_piece] == WHITE ? WP : BP
        add_piece from, pawn
      end
    end

    # --------------------------------------------------

    def validate_make_move_param(move, from, to, moved_piece, side)
      from_rank, to_rank = SQ_TO_R[from], SQ_TO_R[to]
      if pawn?(moved_piece)
        valid_pawn_start = [from_rank, to_rank] == [RANK_2, RANK_4]  || [from_rank, to_rank] == [RANK_7, RANK_5]
        if move.pawn_start? && !valid_pawn_start
          raise Chess::InvalidMoveError.new "move.pawn_start? && !valid_pawn_start"
        end
        if valid_pawn_start && !move.pawn_start?
          raise Chess::InvalidMoveError.new "valid_pawn_start && !#{move.to_s}.pawn_start?"
        end
      end
    end
  end
end
