class PerftDivider

  def self.divide(fen, level)
    puts "-"*77
    puts "fen   : #{fen}\nlevel : #{level}"

    board = Board.parse_fen(fen)
    top_moves = MoveGenerator.new(board, board.side).generate.map(&.move)
#    puts top_moves.map(&.to_s)
    total_leaves = 0
    top_moves.sort_by(&.to_s).each do |top_move|
      board = Board.parse_fen(fen)
      MoveMaker.new(board).make_move top_move
      nof_leaves = Perft.new(board).perft(level-1)
      total_leaves += nof_leaves
#      printf "\n%-16s  %-5s" , [top_move.to_s, nof_leaves]
      printf "\n%-5s %-16s " , [nof_leaves, top_move.to_s]
    end
    puts "\n---"
    printf "%s moves -> %s  leaves \n", [top_moves.size, total_leaves]
  end

end
