module Chess
  class BitBoard
    EMPTY = 0_u64

    getter :value

    def initialize(@value : UInt64 = EMPTY)
    end

    def clone
      self.class.new(value)
    end

    def ==(other)
      other.class == self.class && other.value == value
    end

    BIT_SET_MASKS = StaticArray(UInt64, 64).new {|i| 1_u64 << i }
    BIT_CLR_MASKS = StaticArray(UInt64, 64).new {|i| ~BIT_SET_MASKS[i] }

    def set?(i)
      0 != value & BIT_SET_MASKS[i]
    end

    def set_bit(i)
      @value |= BIT_SET_MASKS[i]
    end

    def clear_bit(i)
      @value &= BIT_CLR_MASKS[i]
    end

    def count_bits
      count = 0
      v = @value
      until v == 0
        v &= v - 1
        count += 1
      end
      count
    end

    # source: https://chessprogramming.wikispaces.com/Looking+for+Magics
    BIT_TABLE = {
    	63, 30, 3, 32, 25, 41, 22, 33, 15, 50, 42, 13, 11, 53, 19, 34, 61, 29, 2,
  	  51, 21, 43, 45, 10, 18, 47, 1, 54, 9, 57, 0, 35, 62, 31, 40, 4, 49, 5, 52,
  	  26, 60, 6, 23, 44, 46, 27, 56, 16, 7, 39, 48, 24, 59, 14, 12, 55, 38, 28,
    	58, 20, 37, 17, 36, 8
  	}

    def pop_bit
      b = value ^ (value - 1)
      fold = 0_u32 + ((b & 0xffffffff) ^ (b >> 32))
      @value &= (value - 1)
      idx = ((fold * 0x783a9b23) >> 26)
      BIT_TABLE[idx]
    end
  end
end
