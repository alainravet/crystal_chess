module Chess
  struct Move
    class Parser
      # candidate : "a2a4", "a7a8Q"
      def self.parse(candidate : String, valid_moves : Array(Move)) : Chess::Move
        return Move::NOMOVE unless [4, 5].includes?(candidate.size)
        valid_moves.each do |m|
          return m if m.to_s_compact == candidate
        end
        Move::NOMOVE
      end
    end
  end
end
