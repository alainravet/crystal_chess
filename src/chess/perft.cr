class Perft
  getter :board
  def self.test_and_print(board, level)
    Debug.print_board(board)
    total_leaves = 0

    MoveGenerator.new(board, board.side).generate.each_with_index do |scored_move, i|
      move = scored_move.move
      next unless MoveMaker.new(board).make_move(move)
      nof_leaves = Perft.new(board).perft(level-1)
      total_leaves +=nof_leaves
      MoveMaker.new(board).take_move
      printf "\nmove %2s   %-12s -> %s  leaves ", [i, move.to_s, nof_leaves]
    end

    puts "\n\nTOTAL : #{total_leaves} leaves found across #{level} plies"
  end

  def initialize(@board)
    @leafNodes = 0_u64
  end

  def perft(depth : Int32)
    if depth == 0
      @leafNodes += 1
# if 0 == @leafNodes % 2_000_000_u64
#   pct =  (100_f64 * @leafNodes) / $expected
#   nof_seconds = (Time.now - $started_at).total_seconds
#   total_expected = (100_f64 * nof_seconds) / pct
#   eta = total_expected - nof_seconds
# # puts "#{@leafNodes} - #{pct.round(2)}% - eta = #{eta} seconds"
#   $log.debug "#{@leafNodes} - #{pct.round(2)}% - eta = #{eta} seconds"
# end

    else
      # moves_list = Chess.moves_lists_pool.get_one
      # moves = MoveGenerator.new(board, board.side, moves_list).generate
      moves = MoveGenerator.new(board, board.side).generate
      moves.each do |scored_move|
        move = scored_move.move
        next unless MoveMaker.new(board).make_move(move)
        perft(depth-1)
        MoveMaker.new(board).take_move
      end
      # Chess.moves_lists_pool.give_back(moves_list)
    end
    @leafNodes
  end
end

$leafNodes = 0
