module Chess
  module Search
    class Searcher
      alias Score = Int32

      MATE = 29000

      def initialize(@board, @search_info)
      end

      # similar to Perft
      def alpha_beta(alpha : Score, beta : Score, depth, do_null=false) : Score
        @search_info.nodes += 1
        if depth == 0
#          Debug.print_compact_board(tab, @board)
          score = Chess::Search::Evaluator.new(@board).evaluate
          return score
        end

        return 0 if @board.repetition_detected?
      # return 0                 if( pos->fiftyMove >= 100)
      # return EvalPosition(pos) if(pos->ply > MAXDEPTH - 1) {

        scored_moves = MoveGenerator.new(@board, @board.side).generate
# puts scored_moves.map{|o| o.score.to_s}.join(" ")
      # scored_moves.sort!{|o| -o.score}

        legal = 0           # if no legal moves found: mate or stale mate
        old_alpha = alpha   # detect if we found a move that beats alpha

        best_move = Move::NOMOVE
        score     = - Int32::MAX

        scored_moves.each do |scored_move|
          move          = scored_move.move
          move_is_legal = MoveMaker.new(@board).make_move move
          next unless move_is_legal
          legal += 1

          score = -alpha_beta(-beta, -alpha, depth-1, true)
          MoveMaker.new(@board).take_move

          if score > alpha         # beat alpha
      			if score >= beta       #   beat the cut-off -> stop searching here
      				if legal==1
      					@search_info.fhf += 1
      				end
      				@search_info.fh += 1
      				return beta   # cut-off
      			else
  			      alpha = score;
		          best_move = scored_move.move
            end
      		end
        end

      	if legal == 0
      		if @board.king_attacked?
      			return -MATE + @board.ply    # mate-in-2 > mate-in-3
      		else
      			return 0
      		end
      	end

      	if alpha != old_alpha            # alpha was improved
          @board.store_pv_move(best_move)
      	end

        alpha
      end

      # What:
      #  - clear stats, heuristic arrays, history, ..
      # WHen:
      #  before a new seach

      # static void ClearForSearch(S_BOARD *pos, S_SEARCHINFO *info) {
      #
      # 	int index = 0;
      # 	int index2 = 0;
      #
      # 	for(index = 0; index < 13; ++index) {
      # 		for(index2 = 0; index2 < BRD_SQ_NUM; ++index2) {
      # 			pos->searchHistory[index][index2] = 0;
      # 		}
      # 	}
      #
      # 	for(index = 0; index < 2; ++index) {
      # 		for(index2 = 0; index2 < MAXDEPTH; ++index2) {
      # 			pos->searchKillers[index][index2] = 0;
      # 		}
      # 	}
      #
      # 	ClearPvTable(pos->PvTable);
      # 	pos->ply = 0;
      #
      # 	info->starttime = GetTimeMs();
      # 	info->stopped = 0;
      # 	info->nodes = 0;
      # 	info->fh = 0;
      # 	info->fhf = 0;
      # }
      #
      # What: tries to find a quiet position, when starting from a capture
      # static int Quiescence(int alpha, int beta, S_BOARD *pos, S_SEARCHINFO *info) {
      # 	return 0;
      # }
      #

      # What: (SearchPosition)
      #   - search init
      #   - iterating deepening

      # void SearchPosition(S_BOARD *pos, S_SEARCHINFO *info) {
      #
      #     int bestMove  = NOMOVE;     # updated at the end of each alpha_beta
      #     int bestScore = -INFINITE;  # updated at the end of each alpha_beta
      #     int currentDepth = 0 ;
      #     int pvMoves = 0;  # nof of moves in the pv line (same as max = get_pv_line in gui)
      #     int pvNum = 0;
      #
      #     ClearForSearch(pos,info);
      #
      #     // iterative deepening

      # for depth 1..MAX :
      #    search AlphaBeta(depth)

      #     for( currentDepth = 1; currentDepth <= info->depth; ++currentDepth ) {
      #         // alpha	 beta
      #         bestScore = AlphaBeta(-INFINITE, INFINITE, currentDepth, pos, info, TRUE);
      #
      #         // missing: handle 'out of time?'
      #
      #         pvMoves = GetPvLine(currentDepth, pos);
      #         bestMove = pos->PvArray[0];             # 1st move in the PV line
      #
      #         printf("Depth:%d score:%d move:%s nodes:%ld ",
      #                currentDepth,bestScore,PrMove(bestMove),info->nodes);
      #
      #         pvMoves = GetPvLine(currentDepth, pos);
      #         printf("pv");
      #         for(pvNum = 0; pvNum < pvMoves; ++pvNum) {
      #             printf(" %s",PrMove(pos->PvArray[pvNum]));
      #         }
      #         printf("\n");
      #         printf("Ordering:%.2f\n",(info->fhf/info->fh));
      #     }
    end
  end
end
