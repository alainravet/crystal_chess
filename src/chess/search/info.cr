# in C : S_SEARCHINFO
module Chess::Search
  class Info
    property \
      :starttime,
      :stoptime,
      :depth,
      :depthset,    # only search till a certain depth
      :timeset,     # "you have 5 minutes", f.ex
      :movestogo,
      :infinite,    # if true, never stop searching, till GUI says to.
      :nodes,       # count of visited positions.
      :quit,        # true when GUI says to quit.
      :stopped,     # true when ?   says to stop.
      :fh,          # fail high
      :fhf          # fail high first

    @nodes  = 0
    @fh     = 0
    @fhf    = 0

    def ordering_pct
      fhf.to_f / fh
    end
  end
end
