module Chess
  module Search
    class Evaluator
      def initialize(@board)
      end

      def evaluate
        score = @board.material[WHITE] - @board.material[BLACK];

        score += score_based_on_piece_position(WP, PAWN_TABLE)
        score += score_based_on_piece_position(BP, PAWN_TABLE)
        score += score_based_on_piece_position(WN, KNIGHT_TABLE)
        score += score_based_on_piece_position(BN, KNIGHT_TABLE)
        score += score_based_on_piece_position(WB, BISHOP_TABLE)
        score += score_based_on_piece_position(BB, BISHOP_TABLE)
        score += score_based_on_piece_position(WR, ROOK_TABLE)
        score += score_based_on_piece_position(BR, ROOK_TABLE)

        @board.side == WHITE ? score : -score
      end

      private def score_based_on_piece_position(piece, table)
        score = 0
        black_piece = 7 <= piece && piece <= 12
        # TODO : TIV if piece_list_clean(piece) is slower
        (0...@board.piece_num[piece]).each do |piece_i|
          sq64 = sq64(@board.piece_list[piece][piece_i])
          if black_piece
            score -= table[mirror_64(sq64)]
          else
            score += table[sq64]
          end
        end
        score
      end

      def mirror_64(sq64)
        MIRROR_64_TABLE[sq64]
      end

      PAWN_TABLE = {
         0,  0,  0,  0,  0,  0,  0,  0,
        10, 10,  0,-10,-10,  0, 10, 10,
         5,  0,  0,  5,  5,  0,  0,  5,
         0,  0, 10, 20, 20, 10,  0,  0,  # centralize
         5,  5,  5, 10, 10,  5,  5,  5,
        10, 10, 10, 20, 20, 10, 10, 10,  # promote
        20, 20, 20, 30, 30, 20, 20, 20,
         0,  0,  0,  0,  0,  0,  0,  0
      }

      KNIGHT_TABLE = {
        0,-10,  0,  0,  0,  0,-10,  0,    # develop the knights
        0,  0,  0,  5,  5,  0,  0,  0,
        0,  0, 10, 10, 10, 10,  0,  0,    # centralize them
        0,  0, 10, 20, 20, 10,  5,  0,
        5, 10, 15, 20, 20, 15, 10,  5,    # advance them
        5, 10, 10, 20, 20, 10, 10,  5,
        0,  0,  5, 10, 10,  5,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0
      }

      BISHOP_TABLE = {
        0,  0,-10,  0,  0,-10,  0,  0,
        0,  0,  0, 10, 10,  0,  0,  0,
        0,  0, 10, 15, 15, 10,  0,  0,
        0, 10, 15, 20, 20, 15, 10,  0,
        0, 10, 15, 20, 20, 15, 10,  0,
        0,  0, 10, 15, 15, 10,  0,  0,
        0,  0,  0, 10, 10,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0
      }

      ROOK_TABLE = {
        0,  0,  5, 10, 10,  5,  0,  0,
        0,  0,  5, 10, 10,  5,  0,  0,
        0,  0,  5, 10, 10,  5,  0,  0,  # centralize
        0,  0,  5, 10, 10,  5,  0,  0,
        0,  0,  5, 10, 10,  5,  0,  0,
        0,  0,  5, 10, 10,  5,  0,  0,
       25, 25, 25, 25, 25, 25, 25, 25,  # rank 7
        0,  0,  5, 10, 10,  5,  0,  0
      }

      MIRROR_64_TABLE = {
        56, 57, 58, 59, 60, 61, 62, 63,
        48, 49, 50, 51, 52, 53, 54, 55,
        40, 41, 42, 43, 44, 45, 46, 47,
        32, 33, 34, 35, 36, 37, 38, 39,
        24, 25, 26, 27, 28, 29, 30, 31,
        16, 17, 18, 19, 20, 21, 22, 23,
         8,  9, 10, 11, 12, 13, 14, 15,
         0,  1,  2,  3,  4,  5,  6,  7
      }

    end
  end
end
