module Chess
  module Search
    struct PvEntry
      property :hash_key, :move

      def initialize(@hash_key : HashKey, @move : Move)
      end
    end

    struct PvTable
      getter :num_entries

      def initialize(@num_entries)
        @entries = Array(PvEntry?).new(@num_entries, nil)
        @num_entries -= 2 # TODO ? is this necessary (from C code)
      end

      def get(key : HashKey) : Move|Nil
        e = @entries[idx_for(key)]
        return e.move if e && key == e.hash_key
        nil
      end

      def store(key : HashKey, move : Move)
        entry = PvEntry.new(key, move)
        @entries[idx_for(key)]= entry
      end

      private def idx_for(key)
        idx = key % @num_entries
      end
    end
  end
end
