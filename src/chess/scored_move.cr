struct ScoredMove
  getter :move, :score

  def initialize(@move : Move, @score : Int32)
  end

  def to_s(show_score = true)
    show_score ?
      "#{move.to_s} - #{score}" :
      move.to_s
  end
end
