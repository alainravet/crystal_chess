module Chess
  module AttackDetector
    extend self

    KNIGHT_ATTACKs_DIRS = { -8, -19, -21, -12, 8, 19, 21, 12 }
    ROOK_ATTACKs_DIRS   = { -1, -10,   1,  10 }
    BISHOP_ATTACKs_DIRS = { -9, -11,  11,   9 }
    KING_ATTACKs_DIRS   = { -1, -10,	1,   10, -9, -11, 11, 9 }

    def square_attacked_by?(b : Chess::Board, sq120 : Cell120Coord, side : PieceColour) : Bool
      # attacked by pawns?
    	if(side == WHITE)
    		return true if b.piece(sq120-11) == WP || b.piece(sq120-9) == WP
    	else
    		return true if b.piece(sq120+11) == BP || b.piece(sq120+9) == BP
    	end

      # attacked by knights?
      (0..7).each do |i|
        piece = b.piece(sq120 + KNIGHT_ATTACKs_DIRS[i])
        next if Piece.offboard?(piece)
        return true if knight?(piece) && Piece.same_side?(piece, side)
      end

      # attacked by king
      (0..7).each do |i|
        piece = b.piece(sq120 + KING_ATTACKs_DIRS[i])
        next if Piece.offboard?(piece)
        if king?(piece) && Piece.same_side?(piece, side)
          return true
        end
      end

      # attacked by rooks or queens
      (0...ROOK_ATTACKs_DIRS.size).each do |i|
        dir = ROOK_ATTACKs_DIRS[i]
      	sq = sq120 + dir
        piece = b.piece(sq)
        while piece != OFFBOARD
      	  if piece != EMPTY
            if rookqueen?(piece) && Piece.same_side?(piece, side)
              return true
            end
            break
          end
          sq += dir
      	  piece = b.piece(sq)
        end
      end

      # attacked by bishops or queens
      (0...BISHOP_ATTACKs_DIRS.size).each do |i|
        dir = BISHOP_ATTACKs_DIRS[i]
      	sq = sq120 + dir
        piece = b.piece(sq)
        while piece != OFFBOARD
      	  if piece != EMPTY
            if bishopqueen?(piece) && Piece.same_side?(piece, side)
              return true
            end
            break
          end
          sq += dir
      	  piece = b.piece(sq)
        end
      end

      false
    end
  end
end
