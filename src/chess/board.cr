module Chess
  class Board
    def self.parse_fen(fen : String) : Chess::Board
      FenParser.parse(fen)
    end

    def self.parse_ascii(ascii : String) : Chess::Board
      AsciiParser.parse(ascii)
    end

  # --------------
    property :side, :en_pas_square, :castle_perms, :hash_key, :fifty_move
    getter :moves
    getter :king_squares, :pawns
    getter :history
    property :ply,    # half-moves in the current search
             :his_ply # half-moves in the entire game
    getter :piece_list, :piece_num, :big_pieces, :maj_pieces, :min_pieces, :material
    getter :pv_array

    @pieces       = Array(ChessPiece      ).new(NOF_CELLS){ Piece::OFFBOARD }
    # 3 = WHITE or BLACK or BOTH :
    @pawns        = Array(Chess::BitBoard ).new(3){ Chess::BitBoard.new }
    # 2 = WHITE or BLACK
    @king_squares = Array(Cell120Coord    ).new(2){ NO_SQ }

    @piece_num  = Array(ChessPiece).new(13) { 0 }
    @piece_list = Array(Array(Cell120Coord)).new(13){
      Array(Int32).new(10, NO_SQ)
    }
    @moves = Array(Move).new(2048)

    @side          = Colour::BOTH
    @en_pas_square = NO_SQ
    @fifty_move    = 0

    @history       = Array(Undo|Nil).new(2048, nil)
    @his_ply       = 0 # TODO : rename to history_index (?)
    @ply           = 0  # nof half-moves since start of the search.

    @castle_perms  = 0b0000
    @hash_key      = 0_u64

    @big_pieces = [0, 0]
    @maj_pieces = [0, 0]
    @min_pieces = [0, 0]
    @material   = [0, 0]

    def initialize()
      (RANK_1..RANK_8).each do |rank|
        (FILE_A..FILE_H).each do |file|
          sq_120 = sq120(file, rank)
          @pieces[sq_120] = Piece::EMPTY
        end
      end
      @pv_table = Search::PvTable.new(PV_TABLE_SIZE)
      # The search only stores the move that beat alpha

      # TODO rename @pv_array to @pv_line
      # TODO test perf if @pv_array is a dynamic array (to simplify usages...)
      @pv_array = [] of Move
      # We fill @pv_array at the end of the search with
      # the moves from the table (pv_table)
    end

    def piece(sq120 : Cell120Coord)
      @pieces[sq120]
    end

    def pieces
      @pieces
    end

    def pieces(r : Range(Cell120Coord, Cell120Coord))
      @pieces[r]
    end

    def set_piece(sq120, val)
      @pieces[sq120] = val
    end

    def clear_square(sq120)
      @pieces[sq120] = EMPTY
    end

    # TODO : add unit test  (only used in tests)
    # TODO : compare the hash keys
    def ==(other)
      other.side ==side &&
      other.pieces == pieces &&
      other.en_pas_square == en_pas_square &&
      other.castle_perms == castle_perms &&
      other.fifty_move == fifty_move &&
      other.hash_key == hash_key &&
      other.king_squares == king_squares &&
      true
      # more : moves counter, ???
    end

    def generate_hashkey : HashKey
      @hash_key = HashKeyGenerator.generate(self)
    end

    def update_lists_materials # TODO : rename (? init list of materials)
                               #  + find where the real (small) updates take place
      BOARD_SQUARES.each do |index|
		    sq = index
  		  piece = @pieces[index]
        next if piece== EMPTY

  		  colour = Piece.colour(piece)

        @big_pieces[colour] += 1 if PIECE_BIG[piece]
        @min_pieces[colour] += 1 if PIECE_MIN[piece]
        @maj_pieces[colour] += 1 if PIECE_MAJ[piece]
        @material[colour]   += PIECE_VAL[piece]

        @piece_list[piece][@piece_num[piece]] = sq
        @piece_num[piece] += 1

        case piece
        when WK
          @king_squares[WHITE] = sq
        when BK
          @king_squares[BLACK] = sq
        when WP
          @pawns[WHITE].set_bit(sq64(sq))
          @pawns[BOTH ].set_bit(sq64(sq))
        when BP
          @pawns[BLACK].set_bit(sq64(sq))
          @pawns[BOTH ].set_bit(sq64(sq))
        end
      end
    end

    def castling_WQ_permitted?;  0 != castle_perms & WQCA  end
    def castling_WK_permitted?;  0 != castle_perms & WKCA  end
    def castling_BQ_permitted?;  0 != castle_perms & BQCA  end
    def castling_BK_permitted?;  0 != castle_perms & BKCA  end

    def piece_list_clean(piece)
       piece_list[piece].first(piece_num[piece])
    end

    def history_clean
      history.first(his_ply)
    end

    def moves_count
      1 + (his_ply / 2)
    end

    def to_fen
      Board::Converter.to_fen(self)
    end

    def repetition_detected?
      r = (his_ply - fifty_move)..his_ply-1
      r.to_a.each do |i|
        next unless h = history[i]
        return true if hash_key == h.hash_key
      end
      false
    end

    def king_attacked?
      other_side = side ^ 1
      king_sq     = king_squares[side]
      AttackDetector.square_attacked_by?(self, king_sq, other_side)
    end

    def store_pv_move(move) #TODO test
      @pv_table.store hash_key, move
    end

    def probe_pv_table #TODO test
      @pv_table.get(hash_key) || Move::NOMOVE
    end

    # Fill @pv_array with a sequence of max `depth` best moves,
    # starting from the current board.
    #TODO : move all the PV code out of board.cr
    def get_pv_line(depth) #TODO rename to fill_pv_line + refactor
      move = probe_pv_table  # what's the best move we found
                             # for the current board?
    	count = 0;
    	while(!move.no_move? && count < depth)
        if valid_move?(move)
          MoveMaker.new(self).make_move move
          @pv_array << move
          # TODO : make @pv_array a dynamic array, so we don't need to play with indexes
          count += 1
        else
          break # illegal move
        end
        move = probe_pv_table
      end
      # clean up/restore the board to its original position
    	while ply > 0
        MoveMaker.new(self).take_move
      end
    	count
    end

    def fill_pv_line_to_s(depth)
      count = get_pv_line(depth)
      pv_array[0..count-1].map {|move|
         move ? move.to_s_compact : "nil"
      }.join(" ")
    end

    # may this move be played by the current side?
    def valid_move?(move)  #was: move_exitst?
      valid_scored_moves = MoveGenerator.new(self, side).generate
      valid_scored_moves.each do |scored_move|
        #TODO TIV : ? optim :
        # next if scored_move.move != move  # TODO TIV
        move_is_legal = MoveMaker.new(self).make_move(move)
        next unless move_is_legal
        MoveMaker.new(self).take_move
        return true if scored_move.move == move
      end
      false
    end
  end
end
