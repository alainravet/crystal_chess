module Chess
  class Board
    module BaseParser
      def parse_board_parts(rows_64, active_side, castling, enpassant, halfmoveclock, moves) : Chess::Board
        board = Board.new

        board.fifty_move = halfmoveclock.to_i

        rows_64.chars.each_with_index do |ch, i64|
          board.set_piece sq120(i64), piece_from(ch)
        end

        raise "invalid side char #{active_side.inspect}" unless %w(w b).index(active_side)
        board.side = active_side== "w" ? WHITE : BLACK

        # "1" + white => 0
        # "1" + black => 1
        # "2" + white => 2
        # "2" + black => 3
        board.his_ply = (moves.to_i - 1) * 2 + (board.side==WHITE ? 0 : 1)
        board.ply     = board.his_ply # ?? really TODO : clean after Search is implemented

        unless enpassant == "-"
          board.en_pas_square = Algebraic.sq120_from(enpassant)
        end

        individual_perms = castling.chars.map{|perm_c|
          case perm_c
            when 'K' then WKCA
            when 'Q' then WQCA
            when 'k' then BKCA
            when 'q' then BQCA
            when '-'
            else raise "BUG: invalid castling perm character: #{perm_c}"
            end
        }.compact
        board.castle_perms = individual_perms.sum

        # halfmoveclock # 0 or n
        # moves # 1
        board.update_lists_materials
        board.generate_hashkey
        board
      end

      def piece_from(char : Char) : ChessPiece
        case char
        when '.' then EMPTY
        when 'p' then BP
        when 'r' then BR
        when 'n' then BN
        when 'b' then BB
        when 'q' then BQ
        when 'k' then BK
        when 'P' then WP
        when 'R' then WR
        when 'N' then WN
        when 'B' then WB
        when 'Q' then WQ
        when 'K' then WK
        else
          raise "Invalid FEN piece character : #{char}"
        end
      end

    end
  end
end
