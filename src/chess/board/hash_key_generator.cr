module Chess
  class Board
    module HashKeyGenerator
      module PreComputedKeys
        alias HashKey     = UInt64

        PIECE_HKEY = Array(Array(HashKey)).new(120){
           Array(HashKey).new(PIECE_SIZE){ Random.new.next_u64 }
        }
        SIDE_HKEY = Random.new.next_u64
        CASTLE_HKEY = Array(HashKey).new(16){
           Random.new.next_u64
        }
      end
      include PreComputedKeys

      def self.generate(board : Chess::Board) : HashKey
        hash_key = 0_u64

        BOARD_SQUARES.each do |index|
          sq = index
          piece = board.piece(index)
          next if piece== EMPTY
          hash_key ^= PIECE_HKEY[sq][piece]
        end

        hash_key ^= SIDE_HKEY if board.side == WHITE

        hash_key ^= CASTLE_HKEY[board.castle_perms];

        if board.en_pas_square != NO_SQ
          hash_key ^= PIECE_HKEY[board.en_pas_square][EMPTY]
        end
        hash_key
      end
    end
  end
end

include Chess::Board::HashKeyGenerator::PreComputedKeys
