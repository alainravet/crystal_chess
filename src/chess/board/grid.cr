module Chess
  class Board
    module Grid
      alias Cell64Coord  = Int32  # 0..63
      alias Cell120Coord = Int32  # 0..119
      alias FileOrRank   = Int32  # 0..7

      NOF_ROWS      = 12
      CELLS_PER_ROW = 10
      NOF_CELLS     = CELLS_PER_ROW * NOF_ROWS

      A1 = 21; B1 = 22; C1 = 23; D1 = 24; E1 = 25; F1 = 26; G1 = 27; H1 = 28
      A2 = 31; B2 = 32; C2 = 33; D2 = 34; E2 = 35; F2 = 36; G2 = 37; H2 = 38
      A3 = 41; B3 = 42; C3 = 43; D3 = 44; E3 = 45; F3 = 46; G3 = 47; H3 = 48
      A4 = 51; B4 = 52; C4 = 53; D4 = 54; E4 = 55; F4 = 56; G4 = 57; H4 = 58
      A5 = 61; B5 = 62; C5 = 63; D5 = 64; E5 = 65; F5 = 66; G5 = 67; H5 = 68
      A6 = 71; B6 = 72; C6 = 73; D6 = 74; E6 = 75; F6 = 76; G6 = 77; H6 = 78
      A7 = 81; B7 = 82; C7 = 83; D7 = 84; E7 = 85; F7 = 86; G7 = 87; H7 = 88
      A8 = 91; B8 = 92; C8 = 93; D8 = 94; E8 = 95; F8 = 96; G8 = 97; H8 = 98
      NO_SQ    = 99

      BOARD_SQUARES = c = (A1..H1).to_a + (A2..H2).to_a + (A3..H3).to_a + (A4..H4).to_a + (A5..H5).to_a + (A6..H6).to_a + (A7..H7).to_a + (A8..H8).to_a

      FILE_A = 0; FILE_B = 1; FILE_C = 2; FILE_D = 3; FILE_E = 4; FILE_F = 5; FILE_G = 6; FILE_H = 7;
      RANK_1 = 0; RANK_2 = 1; RANK_3 = 2; RANK_4 = 3; RANK_5 = 4; RANK_6 = 5; RANK_7 = 6; RANK_8 = 7;

      module PreComputed
        COORD_120_TO_64 = StaticArray(Cell64Coord, NOF_CELLS).new(){ 65 }
        COORD_64_TO_120 = StaticArray(Cell120Coord, 64      ).new(){ 120 }
        SQ_TO_R  = Array.new(120, OFFBOARD)
        SQ_TO_F  = Array.new(120, OFFBOARD)
        # precompute:
        (FILE_A..FILE_H).each do |file|
          (RANK_1..RANK_8).each do |rank|
            sq_120 = sq120(file, rank)
            sq_64  = file + (rank * 8)
            COORD_120_TO_64[sq_120] = sq_64
            COORD_64_TO_120[sq_64 ] = sq_120
            SQ_TO_F[sq_120] = file
            SQ_TO_R[sq_120] = rank
          end
        end
      end

      module Utils
        include PreComputed
        def sq64(index : Cell120Coord) : Cell64Coord
          COORD_120_TO_64[index]
        end

        def sq120(index : Cell64Coord)  : Cell120Coord
          COORD_64_TO_120[index]
        end

        def sq120(file, rank : FileOrRank) : Cell120Coord
          21 + file + (rank * CELLS_PER_ROW)
        end
      end
    end
  end
end

include Chess::Board::Grid
include Chess::Board::Grid::Utils  # to make #sq120 global - TODO : investigate/refactor
