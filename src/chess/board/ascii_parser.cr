require "./base_parser"
module Chess
  class Board
    module AsciiParser
      extend BaseParser
      def self.parse(ascii) : Chess::Board
        # ascii = "
        # rnbqkbnr
        # pppppppp
        # ........
        # ........
        # ........
        # ........
        # PPPPPPPP
        # RNBQKBNR
        # w KQkq - 0 1
        # "
        parts =    ascii.strip.split("\n").map(&.gsub(/\#.*/, "")).map(&.strip)
        all   = [parts[0..7].reverse.join,  parts[8..10].join(" ")].join(" ")
        rows_64, active_side, castling, enpassant, halfmoveclock, moves = all.split

        parse_board_parts(rows_64, active_side, castling, enpassant, halfmoveclock, moves)
      end

    end
  end
end
