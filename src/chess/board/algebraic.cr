module Chess
  class Board
    module Algebraic
      extend self

      CHAR_FOR_RANK   = "12345678"
      CHAR_FOR_FILE   = "abcdefgh"

      # Ex: "a5" -> {0, 4}
      def file_rank_from(algebraic_position): {FileOrRank, FileOrRank}
    		file_c, rank_c = algebraic_position.chars
    		file = file_c - CHAR_FOR_FILE.chars.first
    		rank = rank_c - CHAR_FOR_RANK.chars.first
        {file, rank}
      end

      # Ex: "a1" -> 21
      def sq120_from(algebraic_position) : Cell120Coord
        file, rank = file_rank_from(algebraic_position)
        sq120(file, rank)
      end

      def algebraic_for(sq120 : Cell120Coord)
        rank = (sq120 - 21) / 10
        file = (sq120 - 21) % 10
        "#{CHAR_FOR_FILE[file]}#{CHAR_FOR_RANK[rank]}"
      end
    end
  end
end
include Chess::Board::Algebraic
