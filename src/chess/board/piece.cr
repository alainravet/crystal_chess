module Chess
  module Piece
    extend self

    def offboard?(piece) piece == OFFBOARD  end

    def empty?(piece)    piece == EMPTY     end

    def same_side?(piece, side)
      PIECE_COLOUR[piece] == side
    end

    def white?(piece)
      PIECE_COLOUR[piece] == WHITE
    end

    def black?(piece)
      PIECE_COLOUR[piece] == BLACK
    end


    alias PieceColour = Int32
    module Colour
      WHITE = 0
      BLACK = 1
      BOTH  = 2

      def self.white?(side) side == WHITE end
      def self.black?(side) side == BLACK end
    end

    def colour(piece)
      PIECE_COLOUR[piece]
    end


    alias ChessPiece = Int32
    PIECE_SIZE = 13 # for arrays indexed by pieces

    EMPTY =  0
    OFFBOARD = 100

    WP    =  1
    WN    =  2
    WB    =  3
    WR    =  4
    WQ    =  5
    WK    =  6
    BP    =  7
    BN    =  8
    BB    =  9
    BR    = 10
    BQ    = 11
    BK    = 12

    LOOP_SLIDE_PIECE      = { {WB, WR, WQ}, {BB, BR, BQ}  }
    LOOP_NON_SLIDE_PIECE  = { {WN, WK},     {BN, BK}      }

    def all_sliding_for(side : PieceColour)
      LOOP_SLIDE_PIECE[side]
    end

    def all_non_sliding_for(side : PieceColour)
      LOOP_NON_SLIDE_PIECE[side]
    end


    CHAR_FOR_PIECE  = ".PNBRQKpnbrqk"

    def char_for_piece(piece)
      CHAR_FOR_PIECE[piece]
    end
    ALL = WP..BK

    module PreComputed
      PIECE_COLOUR = { BOTH,
                      WHITE, WHITE, WHITE, WHITE, WHITE, WHITE,
      	              BLACK, BLACK, BLACK, BLACK, BLACK, BLACK }

      PIECE_BIG = { false, false, true,  true,  true,  true,  true,  false, true,  true,  true,  true,  true  }
      PIECE_MAJ = { false, false, false, false, true,  true,  true,  false, false, false, true,  true,  true  }
      PIECE_MIN = { false, false, true,  true,  false, false, false, false, true,  true,  false, false, false }
      PIECE_VAL = { 0,     100,   325,   325,   550,   1000,  50_000,
                           100,   325,   325,   550,   1000,  50_000 }

      PIECE_IS_KNIGHT       = { false, false, true,  false, false, false, false, false, true,  false, false, false, false }
      PIECE_IS_KING         = { false, false, false, false, false, false, true,  false, false, false, false, false, true  }
      PIECE_IS_ROOKQUEEN    = { false, false, false, false, true,  true,  false, false, false, false, true,  true,  false }
      PIECE_IS_BISHOPQUEEN  = { false, false, false, true,  false, true,  false, false, false, true,  false, true,  false }
      PIECE_SLIDES          = { false, false, false, true,  true,  true,  false, false, false, true,  true,  true,  false }

      def knight?(piece : ChessPiece)
        PIECE_IS_KNIGHT[piece]
      end

      def king?(piece : ChessPiece)
        PIECE_IS_KING[piece]
      end

      def rookqueen?(piece : ChessPiece)
        PIECE_IS_ROOKQUEEN[piece]
      end

      def bishopqueen?(piece : ChessPiece)
        PIECE_IS_BISHOPQUEEN[piece]
      end

      def pawn?(piece) !PIECE_BIG[piece] end
      def big?(piece)   PIECE_BIG[piece] end
      def min?(piece)   PIECE_MIN[piece] end
      def maj?(piece)   PIECE_MAJ[piece] end
    end
  end
end

include Chess::Piece::Colour  # WHITE, BLACK, BOTH
include Chess::Piece
include Chess::Piece::PreComputed
