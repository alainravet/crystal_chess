#FEN_2 = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b k e3 3 5"
#rows, active_side, castling, enpassant, halfmoveclock, moves = fen.split

module Chess
  class Board
    class Converter
      def self.to_fen(board)
        Converter.new(board).to_fen
      end

      getter :board

      def initialize(@board)
      end

      def to_fen
        [rows_part,
          active_side_part, castlings_part, enpassant_part,
          halfmoveclock_part, clock_part
        ].join(" ")
      end

      def rows_part
        res = (RANK_1..RANK_8).to_a.reverse.map do |rank|
          (FILE_A..FILE_H).map do |file|
            sq120 = sq120(file, rank)
            piece  = board.piece(sq120)
            Piece.char_for_piece(piece)
          end.join
        end.join("\/")
        res = res
          .gsub("."*8, "8")
          .gsub("."*7, "7")
          .gsub("."*6, "6")
          .gsub("."*5, "5")
          .gsub("."*4, "4")
          .gsub("."*3, "3")
          .gsub("."*2, "2")
          .gsub("."*1, "1")
      end

      def active_side_part
        (board.side == WHITE) ? "w" : "b"
      end

      def castlings_part
        #KQkq
        res = ""
        res += "K" if board.castling_WK_permitted?
        res += "Q" if board.castling_WQ_permitted?
        res += "k" if board.castling_BK_permitted?
        res += "q" if board.castling_BQ_permitted?
        res
      end

      def enpassant_part
        ep = board.en_pas_square
        ep == NO_SQ ? "-" : Algebraic.algebraic_for(ep)
      end

      def halfmoveclock_part
        board.fifty_move.to_s
      end

      def clock_part
        board.moves_count.to_s
      end
    end
  end
end
