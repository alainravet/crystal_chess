require "./base_parser"
module Chess
  class Board
    module FenParser
      extend BaseParser
      def self.parse(fen) : Chess::Board
        board = Board.new
        rows, active_side, castling, enpassant, halfmoveclock, moves = fen.split

        rows_64 = rows
          .split("/").map(&.reverse).join.reverse
          .gsub("8","."*8).gsub("7","."*7)
          .gsub("6","."*6).gsub("5","."*5).gsub("4","."*4)
          .gsub("3","."*3).gsub("2","."*2).gsub("1","."*1)
        parse_board_parts rows_64, active_side, castling, enpassant, halfmoveclock, moves
      end
    end
  end
end
