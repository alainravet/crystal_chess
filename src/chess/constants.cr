module Chess
  FEN_START_POSITION = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

  module Constants
    # CastlingPermissions
    WKCA = 0b0001
    WQCA = 0b0010
    BKCA = 0b0100
    BQCA = 0b1000
  end
end
