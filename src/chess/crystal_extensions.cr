module Random
  def next_u64
    (next_u32.to_u64 << 32) + next_u32
  end
end
