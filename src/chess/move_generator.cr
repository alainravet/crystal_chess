module Chess
  class MoveGenerator
    getter :board, :side, :moves

    def initialize(@board, @side, moves = nil)
      @moves = moves || MovesList.new
    end


    def other_side
      @_other_side ||= side ^ 1
    end

    def add_quiet_move(move : Move)
      moves << ScoredMove.new(move, 0)
    end

    def add_capture_move(move : Move)
      moves << ScoredMove.new(move, 0)
    end

    def generate : MovesList
      if Piece::Colour.white?(side)
        generate_white_pawn_moves
      elsif Piece::Colour.black?(side)
        generate_black_pawn_moves
      end

      if Piece::Colour.white?(side)
        if Rules.may_castle_WK?(board)
          add_quiet_move Move.assemble(E1, G1, EMPTY, EMPTY, Move::MFLAGCA)
        end
        if Rules.may_castle_WQ?(board)
          add_quiet_move Move.assemble(E1, C1, EMPTY, EMPTY, Move::MFLAGCA)
        end

      elsif Piece::Colour.black?(side)
        if Rules.may_castle_BK?(board)
          add_quiet_move Move.assemble(E8, G8, EMPTY, EMPTY, Move::MFLAGCA)
        end
        if Rules.may_castle_BQ?(board)
          add_quiet_move Move.assemble(E8, C8, EMPTY, EMPTY, Move::MFLAGCA)
        end
      end

      Piece.all_sliding_for(side).each do |piece|
        nof_pieces = board.piece_num[piece]
        nof_dir    = NUM_DIR[piece]
        (0...nof_pieces).each do |piece_i|
          from_sq = board.piece_list[piece][piece_i]
          (0...nof_dir).each do |dir_i|
            dir = PIECE_DIR[piece][dir_i]
            to_sq = from_sq + dir
            while !Piece.offboard?(board.piece(to_sq))
              to_piece = board.piece(to_sq)
              if Piece.empty?(to_piece)
                move = Move.assemble(from_sq, to_sq)
                add_quiet_move move
              else
                if enemy_piece?(to_piece)
                  move = Move.assemble(from_sq, to_sq, to_piece)
                  add_capture_move move
                end
                break  # sliding is interruped by obstacle
              end
              to_sq += dir
            end
          end
        end
      end

      Piece.all_non_sliding_for(side).each do |piece|
        nof_pieces = board.piece_num[piece]
        nof_dir    = NUM_DIR[piece]
        (0...nof_pieces).each do |piece_i|
          from_sq = board.piece_list[piece][piece_i]
          (0...nof_dir).each do |dir_i|
            dir      = PIECE_DIR[piece][dir_i]
            to_sq    = from_sq + dir
            to_piece = board.piece(to_sq)
            next if Piece.offboard?(to_piece)
            if Piece.empty?(to_piece)
              add_quiet_move Move.assemble(from_sq, to_sq)
            else
              if enemy_piece?(to_piece)
                add_capture_move Move.assemble(from_sq, to_sq, to_piece)
              end
            end
          end
        end
      end

      moves
    end

    def generate_white_pawn_moves
      (0...board.piece_num[WP]).each do |idx|
        sq120 = board.piece_list[WP][idx]

        # step forward
        t1, t2 = sq120+10, sq120+20
        p1 = board.piece(t1)
        if p1 == EMPTY
          add_white_pawn_move_or_promotion sq120, t1
          if SQ_TO_R[sq120] == RANK_2 && board.piece(t2) == EMPTY
            m = Move.assemble(sq120, t2, EMPTY, EMPTY, Move::MFLAGPS)
            add_quiet_move m
          end
        end

        # capture (simple or en-passant)
        [sq120+9, sq120+11].each do |to_sq|
          cap_piece = board.piece(to_sq)
          next if Piece.offboard?(cap_piece)
          if PIECE_COLOUR[cap_piece] == BLACK
       	    add_white_pawn_cap_move sq120, to_sq, cap_piece
          elsif to_sq == board.en_pas_square
            # TODO : add_en_passant_move
            m = Move.assemble(sq120, to_sq, EMPTY, EMPTY, Move::MFLAGEP)
            add_capture_move m
          end
        end
      end
    end

    def generate_black_pawn_moves
      (0...board.piece_num[BP]).each do |idx|
        sq120 = board.piece_list[BP][idx]
        # step forward
        t1, t2 = sq120-10, sq120-20
        p1 = board.piece(t1)
        if p1 == EMPTY
          add_black_pawn_move_or_promotion sq120, t1
          if SQ_TO_R[sq120] == RANK_7 && board.piece(t2) == EMPTY
            m = Move.assemble(sq120, t2, EMPTY, EMPTY, Move::MFLAGPS)
            add_quiet_move m
          end
        end

        # capture (simple or en-passant)
        [sq120-9, sq120-11].each do |to_sq|
          cap_piece = board.piece(to_sq)
          next if Piece.offboard?(cap_piece)
          if Piece.white?(cap_piece)
     	      add_black_pawn_cap_move sq120, to_sq, cap_piece
          elsif to_sq == board.en_pas_square
            # TODO : add_en_passant_move
            m = Move.assemble(sq120, to_sq, EMPTY, EMPTY, Move::MFLAGEP)
            add_capture_move m
          end
        end
      end
    end

    def enemy_piece?(piece)
      Piece.same_side?(piece, other_side)
    end

    def add_white_pawn_move_or_promotion(from, to)
      can_promote = SQ_TO_R[from] == RANK_7
      can_promote ?
        add_white_promotion_moves(from, to) :
        add_quiet_move Move.assemble(from, to)
    end

    def add_white_pawn_cap_move(from, to, cap)
      can_promote = SQ_TO_R[from] == RANK_7
    	can_promote ?
        add_white_promotion_moves(from, to, cap) :
        add_capture_move Move.assemble(from, to, cap)
    end

    def add_white_promotion_moves(from, to, cap = EMPTY)
      [WQ, WR, WB, WN].each do |promotion|
        add_quiet_move Move.assemble(from, to, cap, promotion)
      end
    end

    def add_black_pawn_move_or_promotion(from, to)
      can_promote = SQ_TO_R[from] == RANK_2
      can_promote ?
        add_black_promotion_moves(from, to) :
        add_quiet_move Move.assemble(from, to)
    end

    def add_black_pawn_cap_move(from, to, cap)
      can_promote = SQ_TO_R[from] == RANK_2
    	can_promote ?
        add_black_promotion_moves(from, to, cap) :
        add_capture_move Move.assemble(from, to, cap)
    end

    def add_black_promotion_moves(from, to, cap = EMPTY)
      [BQ, BR, BB, BN].each do |promotion|
        add_quiet_move Move.assemble(from, to, cap, promotion)
      end
    end

    PIECE_DIR = {
    	{  0,   0,   0,   0,   0,   0,   0 },

    	{  0,   0,   0,   0,   0,   0,   0     }, # WP
    	{ -8, -19, -21, -12,   8,  19,  21, 12 }, # WN
    	{ -9, -11,  11,   9,   0,   0,   0,  0 }, # WB
    	{ -1, -10,	 1,  10,   0,   0,   0,  0 }, # WR
    	{ -1, -10,	 1,  10,  -9, -11,  11,  9 }, # WQ
    	{ -1, -10,	 1,  10,  -9, -11,  11,  9 }, # WK

    	{  0,   0,   0,   0,   0,   0,   0     }, # BP
    	{ -8, -19, -21, -12,   8,  19,  21, 12 },
    	{ -9, -11,  11,   9,   0,   0,   0,  0 },
    	{ -1, -10,	 1,  10,   0,   0,   0,  0 },
    	{ -1, -10,	 1,  10,  -9,  -11, 11,  9 },
    	{ -1, -10,	 1,  10,  -9,  -11, 11,  9 }
    }

    NUM_DIR = {0,
               0, 8, 4, 4, 8, 8,
               0, 8, 4, 4, 8, 8}
   end
 end
