module Chess
  module Debug
    extend self

    CHAR_FOR_SIDE   = "wb-"

    def print_all(b : Chess::Board)
      print_board(b)
      print_bitboard(b.pawns[WHITE], "white")
      print_bitboard(b.pawns[BLACK], "black")
      print_bitboard(b.pawns[BOTH ], "both")
    end

    def print_moves(board : Chess::Board)
      moves = MoveGenerator.new(board, board.side).generate
      puts "\n"
      puts "#{moves.size} moves generated:"
      moves.each do |scored_move|
        move  = scored_move.move
        piece = board.piece(move.from)
        puts "-  #{Piece.char_for_piece(piece)} #{move.to_s}"
      end
    end

    def print_compact_board(tab, b : Chess::Board, show_details=false)
      (RANK_1..RANK_8).to_a.reverse.each do |rank|
        s = " #{CHAR_FOR_RANK[rank]} "
        (FILE_A..FILE_H).each do |file|
          sq_120 = sq120(file, rank)
			    s += CHAR_FOR_PIECE[b.piece(sq_120)]
		    end
        puts tab + s
      end
      s = "   "
      (FILE_A..FILE_H).each do |file|
        s += CHAR_FOR_FILE[file]
      end
      puts tab + s
    end

    def print_board(b : Chess::Board, show_details=false)
      puts "\nGame Board:\n\n"
      (RANK_1..RANK_8).to_a.reverse.each do |rank|
        s = " #{CHAR_FOR_RANK[rank]} "
        (FILE_A..FILE_H).each do |file|
          sq_120 = sq120(file, rank)
			    s += CHAR_FOR_PIECE[b.piece(sq_120)]
		    end
        puts s
      end
      s = "   "
      (FILE_A..FILE_H).each do |file|
        s += CHAR_FOR_FILE[file]
      end
      puts s
      #puts "side          : #{CHAR_FOR_SIDE[side]}"
      puts "en-passant    : #{b.en_pas_square}"
      puts "fifty_move    : #{b.fifty_move}"
      puts "ply           : #{b.ply}"
      puts "his_ply       : #{b.his_ply}"

      s = ""
      s += "K" if b.castling_WQ_permitted?
      s += "Q" if b.castling_WQ_permitted?
      s += "k" if b.castling_BK_permitted?
      s += "q" if b.castling_BQ_permitted?
      s = "-" if s == ""
      puts "castle_perms  : #{s}"

      puts "hash_key      : #{b.hash_key}"
      return unless show_details
      puts "material   : #{b.material}"
      puts "big_pieces : #{b.big_pieces}"
      puts "min_pieces : #{b.min_pieces}"
      puts "maj_pieces : #{b.maj_pieces}"

      puts "piece_num : #{b.piece_num}"
      puts "piece_list : #{b.piece_list}"
      puts "king_squares : #{b.king_squares}"
    end

    def print_bitboard(bit_board, msg)
      puts "\nbitboard #{msg}"
      (RANK_1..RANK_8).to_a.reverse.each do |rank|
        s = " #{CHAR_FOR_RANK[rank]} "
        (FILE_A..FILE_H).each do |file|
          bit_idx = sq64(sq120(file, rank))
			    s += (bit_board.set?(bit_idx) ? "X" : ".")
		    end
        puts s
      end
    end
  end
end
