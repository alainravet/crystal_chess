module Chess
  module Debug
    extend self

    def verify_board(b : Chess::Board, raise_error=false)
      verify_bitboard(b, WHITE, "white")
      verify_bitboard(b, BLACK, "black")
      verify_bitboard_both(b)
      check_pieces_lists(b)
      check_pieces_counts(b)
      check_vars(b)

      true
    rescue e
      raise_error ?
        raise e :
        false
    end

    def check_vars(b : Board)
      raise "BUG :invalid side #{b.side}" unless [WHITE, BLACK].index(b.side)
      # raise "invalid hash key" unless b.hash_key == b.generate_hashkey
      unless b.en_pas_square == NO_SQ
        rank = SQ_TO_R[b.en_pas_square]
        if b.side==WHITE && rank != RANK_6
          raise "W enpas : side is white but rank == #{rank} (should be 6)"
        end
        if b.side==BLACK && rank != RANK_3
          raise "B enpas : side is black but rank == #{rank} (should be 3)"
        end
      end
      #
      raise "BUG W king square" unless b.piece(b.king_squares[WHITE]) == WK
      raise "BUG B king square" unless b.piece(b.king_squares[BLACK]) == BK
    end

    def verify_bitboard(b : Chess::Board, colour : PieceColour, msg : String )
      piece = colour == WHITE ? WP : BP

      unless b.pawns[colour].count_bits == b.piece_num[piece]
        s = "expected #{b.pawns[colour].count_bits} == #{b.piece_num[WP]}"
        raise "BUG: #{msg} bitboard #{s} \n\t#{b.pawns[colour].value.to_s(2)}"
      end

      bitboard = b.pawns[colour].clone
      while (0 < bitboard.value)
    		sq64 = bitboard.pop_bit
        raise "invalid #{msg} bit(#{sq64})" unless b.piece(sq120(sq64)) == piece
    	end
    end

    def verify_bitboard_both(b : Chess::Board)
      nof_pawns = b.piece_num[WP] + b.piece_num[BP]
      unless b.pawns[BOTH].count_bits == nof_pawns
        s = "expected #{b.pawns[BOTH].count_bits} == #{nof_pawns}"
        raise "BUG: BOTH bitboard #{s} \n\t#{b.pawns[BOTH].value.to_s(2)}"
      end

      bitboard = b.pawns[BOTH].clone
      while (0 < bitboard.value)
    		sq64 = bitboard.pop_bit
        unless [WP, BP].index(b.piece(sq120(sq64)))
          raise "invalid BOTH bit(#{sq64}) : not a WP nor a BP"
        end
    	end
    end

    def check_pieces_lists(b : Chess::Board)
      # TODO : OOify pieces lists and simplify code
      Piece::ALL.each do |piece|
        (0...b.piece_num[piece]).each do |i|
          sq120 = b.piece_list[piece][i]
          unless b.piece(sq120) == piece
            s = "piece_list[#{piece}][#{i}] == #{b.piece(sq120)} != #{piece}"
            raise "piece list error : #{s}"
          end
        end
      end
    end

    def check_pieces_counts(b : Board)
      material, big_pieces, min_pieces, maj_pieces = [0, 0], [0, 0], [0, 0], [0, 0]
      pieces_counts = Array.new(13, 0)
      (0..63).each do |sq64|
        sq120 = sq120(sq64)
        piece = b.piece(sq120)
        pieces_counts[piece]+= 1
        piece_count = b.piece_num[piece]
        colour = Piece.colour(piece)
        big_pieces[colour] += 1  if PIECE_BIG[piece]
        min_pieces[colour] += 1  if PIECE_MIN[piece]
        maj_pieces[colour] += 1  if PIECE_MAJ[piece]
        material[colour]   += PIECE_VAL[piece] unless colour == BOTH
      end
      raise "BUG W material" unless material[WHITE] == b.material[WHITE]
      raise "BUG B material" unless material[BLACK] == b.material[BLACK]
      raise "BUG W bigPce" unless big_pieces[WHITE] == b.big_pieces[WHITE]
      raise "BUG B bigPce" unless big_pieces[BLACK] == b.big_pieces[BLACK]
      raise "BUG W minPce" unless min_pieces[WHITE] == b.min_pieces[WHITE]
      raise "BUG B minPce" unless min_pieces[BLACK] == b.min_pieces[BLACK]
      raise "BUG W majPce" unless maj_pieces[WHITE] == b.maj_pieces[WHITE]
      raise "BUG B majPce" unless maj_pieces[BLACK] == b.maj_pieces[BLACK]
      # TODO: test below is duplicate/unused TIV
      Piece::ALL.each do |piece|
        unless pieces_counts[piece] == b.piece_num[piece]
          raise "BUG piece num[#{piece}] : #{pieces_counts[piece]} == #{b.piece_num[piece]}"
        end
      end
    end
  end
end
