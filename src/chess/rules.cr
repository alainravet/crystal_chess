module Chess
  module Rules
    extend self

    def may_castle_WK?(board)
      board.castling_WK_permitted? &&
        board.piece(F1) == EMPTY && board.piece(G1) == EMPTY &&
        !AttackDetector.square_attacked_by?(board, E1, BLACK) &&
        !AttackDetector.square_attacked_by?(board, F1, BLACK) &&
        !AttackDetector.square_attacked_by?(board, G1, BLACK)
    end

    def may_castle_WQ?(board)
      board.castling_WQ_permitted? &&
        board.piece(B1) == EMPTY &&
        board.piece(C1) == EMPTY && board.piece(D1) == EMPTY  &&
        !AttackDetector.square_attacked_by?(board, E1, BLACK) &&
        !AttackDetector.square_attacked_by?(board, D1, BLACK) &&
        !AttackDetector.square_attacked_by?(board, C1, BLACK)
    end


    def may_castle_BK?(board)
      board.castling_BK_permitted? &&
        board.piece(F8) == EMPTY && board.piece(G8) == EMPTY &&
        !AttackDetector.square_attacked_by?(board, E8, WHITE) &&
        !AttackDetector.square_attacked_by?(board, F8, WHITE) &&
        !AttackDetector.square_attacked_by?(board, G8, WHITE)
    end

    def may_castle_BQ?(board)
      board.castling_BQ_permitted? &&
        board.piece(B8) == EMPTY &&
        board.piece(C8) == EMPTY && board.piece(D8) == EMPTY  &&
        !AttackDetector.square_attacked_by?(board, E8, WHITE) &&
        !AttackDetector.square_attacked_by?(board, D8, WHITE) &&
        !AttackDetector.square_attacked_by?(board, C8, WHITE)
    end
  end
end
