module Chess
  struct Undo   # TODO : rename to State, or ???
    property :move, :castle_perms, :en_pas_square, :fifty_move, :hash_key

    def initialize(
      @move           : Move,
      @castle_perms   : Int32,
      @en_pas_square  : Cell120Coord,
      @fifty_move     : Int32,
      @hash_key       : HashKey
    )
    end
  end
end
