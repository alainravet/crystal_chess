# /*
# 0000 0000 0000 0000 0000 0111 1111 -> From 0x7F
# 0000 0000 0000 0011 1111 1000 0000 -> To >> 7, 0x7F
# 0000 0000 0011 1100 0000 0000 0000 -> Captured >> 14, 0xF
# 0000 0000 0100 0000 0000 0000 0000 -> EP 0x40000
# 0000 0000 1000 0000 0000 0000 0000 -> Pawn Start 0x80000
# 0000 1111 0000 0000 0000 0000 0000 -> Promoted Piece >> 20, 0xF
# 0001 0000 0000 0000 0000 0000 0000 -> Castle 0x1000000
# */
#
module Chess
  class InvalidMoveError < Exception ; end

  struct Move
    NOMOVE = Move.new(0_u64)

    property :value

    def initialize(@value : UInt64 = 0_u64) ;
    end

    def self.assemble(
      from      : Cell120Coord,
      to        : Cell120Coord,
      captured  : ChessPiece  = EMPTY,
      promotion : ChessPiece  = EMPTY,
      flags     : UInt64      = 0_u64
    ) : Move
      v = from | (to << 7) | ( captured << 14 ) | ( promotion << 20 ) | flags
      self.new(v.to_u64)
    end

    MFLAGEP   = 0x40000_u64       # en-passant
    MFLAGPS   = 0x80000_u64       # rank 2->4 or rank 7->5
    MFLAGCA   = 0x1000000_u64     # castling

    MFLAGCAP  = 0x7C000_u64       # capture
    MFLAGPROM = 0xF00000_u64      # promotion

    def from() : Cell120Coord
      (@value & 0x7F).to_i32
    end

    def to() : Cell120Coord
      ((@value >> 7) & 0x7F).to_i32
    end

    def captured_piece() : ChessPiece
      ((@value >> 14) & 0xF).to_i32
    end

    def promoted_piece() : ChessPiece
      ((@value >> 20) & 0xF).to_i32
    end

    def capture?
      0_u64 != (@value & MFLAGCAP)
    end

    def en_passant?
      0_u64 != (@value & MFLAGEP)
    end

    def promotion?
      0_u64 != (@value & MFLAGPROM)
    end

    def pawn_start?
      0_u64 != (@value & MFLAGPS)
    end

    def castling?
      0_u64 != (@value & MFLAGCA)
    end

    def no_move?
      self == NOMOVE
    end

    def to_s
      return "NOMOVE" if self == NOMOVE
      s = "#{algebraic_for(from)}#{algebraic_for(to)}"
      s+= "/ps" if pawn_start?
      s+= "/cas" if castling?
      s+= "/cap:#{char_for_piece(captured_piece)}" if capture?
      s+= "/->#{char_for_piece(promoted_piece)}" if promotion?
      s
    end

    def to_s_compact
      return "NOMOVE" if self == NOMOVE
      s = "#{algebraic_for(from)}#{algebraic_for(to)}"
      s+= char_for_piece(promoted_piece) if promotion?
      s
    end
  end
end
