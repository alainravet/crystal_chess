require "./utils/**"
require "./chess/**"
include Chess::Constants  # for brevity

module Chess
  def self.moves_lists_pool
    @@moves_lists_pool ||= Utils::Pool(MovesList).new
  end

  def self.reset_before_test
    @@moves_lists_pool = Utils::Pool(MovesList).new
  end
end
