require "benchmark"
struct Scored
  property :score ;
  def initialize(@score) ; end
end


sc_1 = Scored.new(1); sc_2 = Scored.new(2); sc_3 = Scored.new(3); sc_4 = Scored.new(4); sc_5 = Scored.new(5); sc_6 = Scored.new(6); sc_7 = Scored.new(7); sc_8 = Scored.new(8); sc_9 = Scored.new(9); sc_10 = Scored.new(10); sc_11 = Scored.new(11); sc_12 = Scored.new(12); sc_13 = Scored.new(13); sc_14 = Scored.new(14); sc_15 = Scored.new(15); sc_16 = Scored.new(16); sc_17 = Scored.new(17); sc_18 = Scored.new(18); sc_19 = Scored.new(19); sc_20 = Scored.new(20); sc_21 = Scored.new(21); sc_22 = Scored.new(22); sc_23 = Scored.new(23); sc_24 = Scored.new(24); sc_25 = Scored.new(25); sc_26 = Scored.new(26); sc_27 = Scored.new(27); sc_28 = Scored.new(28); sc_29 = Scored.new(29); sc_30 = Scored.new(30); sc_31 = Scored.new(31); sc_32 = Scored.new(32); sc_33 = Scored.new(33); sc_34 = Scored.new(34); sc_35 = Scored.new(35)
arr = [sc_34, sc_3, sc_13, sc_28, sc_16, sc_20, sc_26, sc_32, sc_2, sc_15, sc_12, sc_27, sc_8, sc_6, sc_23, sc_1, sc_17, sc_18, sc_19, sc_29, sc_24, sc_10, sc_25, sc_21, sc_7, sc_11, sc_5, sc_9, sc_4, sc_33, sc_35, sc_14, sc_30, sc_22, sc_31]

Benchmark.ips do |x|
  x.report("sort_by(&.score)") {
    arr = [sc_34, sc_3, sc_13, sc_28, sc_16, sc_20, sc_26, sc_32, sc_2, sc_15, sc_12, sc_27, sc_8, sc_6, sc_23, sc_1, sc_17, sc_18, sc_19, sc_29, sc_24, sc_10, sc_25, sc_21, sc_7, sc_11, sc_5, sc_9, sc_4, sc_33, sc_35, sc_14, sc_30, sc_22, sc_31]
    arr.sort_by(&.score)
  }
  x.report("sort_by!(&.score)") {
    arr = [sc_34, sc_3, sc_13, sc_28, sc_16, sc_20, sc_26, sc_32, sc_2, sc_15, sc_12, sc_27, sc_8, sc_6, sc_23, sc_1, sc_17, sc_18, sc_19, sc_29, sc_24, sc_10, sc_25, sc_21, sc_7, sc_11, sc_5, sc_9, sc_4, sc_33, sc_35, sc_14, sc_30, sc_22, sc_31]
    arr.sort_by!(&.score)
  }
  x.report("sort") {
    arr = [sc_34, sc_3, sc_13, sc_28, sc_16, sc_20, sc_26, sc_32, sc_2, sc_15, sc_12, sc_27, sc_8, sc_6, sc_23, sc_1, sc_17, sc_18, sc_19, sc_29, sc_24, sc_10, sc_25, sc_21, sc_7, sc_11, sc_5, sc_9, sc_4, sc_33, sc_35, sc_14, sc_30, sc_22, sc_31]
    arr.sort{|o| o.score }
  }
  x.report("sort!") {
    arr = [sc_34, sc_3, sc_13, sc_28, sc_16, sc_20, sc_26, sc_32, sc_2, sc_15, sc_12, sc_27, sc_8, sc_6, sc_23, sc_1, sc_17, sc_18, sc_19, sc_29, sc_24, sc_10, sc_25, sc_21, sc_7, sc_11, sc_5, sc_9, sc_4, sc_33, sc_35, sc_14, sc_30, sc_22, sc_31]
    arr.sort!{|o| o.score }
  }
end
