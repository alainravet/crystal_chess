require "./load_all"
include Chess

require "benchmark"
# fen  = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1"
# PerftDivider.divide(fen, 3)
#
# fen  = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/5Q1p/PPPBBPPP/RN2K2R b KQkq - 1 1"
# PerftDivider.divide(fen, 2)

fen = "r3k2r/pbppqpb1/1n2pnp1/3PN3/1p2P3/5Q1p/PPPBBPPP/RN2K2R w KQkq - 2 2 "
PerftDivider.divide(fen, 1)
