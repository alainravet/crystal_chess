require "./../spec_helper"

describe Undo do
  it "holds some properties" do
    mo = Move.assemble(A1, A2)
    cp = 0b1010
    ep = A3
    fm = 42
    hk = 1234_u64

    Undo.new(mo, cp, ep, fm, hk).tap do |m|
      m.move          .should eq(mo)
      m.castle_perms  .should eq(cp)
      m.en_pas_square .should eq(ep)
      m.fifty_move    .should eq(fm)
      m.hash_key      .should eq(hk)
    end
  end
end
