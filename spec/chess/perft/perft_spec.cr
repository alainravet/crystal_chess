require "./../../spec_helper"

describe "Perft" do
  # rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ;D1 20 ;D2 400 ;D3 8902 ;D4 197281 ;D5 4865609 ;D6 119060324
  example "standard position - 1, 2, and 3 levels deep" do
    b = Board.parse_fen(FEN_START_POSITION)
    # Finished in 223.51 milliseconds

    # Perft.new(b).perft(1).should eq(20)
    # Finished in 262.15 milliseconds

    # Perft.new(b).perft(2).should eq(400)
    # Finished in 223.29 milliseconds

    # Perft.new(b).perft(3).should eq(8902)
    # Finished in 248.45 milliseconds

    Perft.new(b).perft(4).should eq(197_281)
    # Finished in 726.13 milliseconds

    # Perft.new(b).perft(5).should eq(4_865_609)
    # Finished in 11.91 seconds

    # Perft.new(b).perft(6).should eq(119_060_324)
    # Finished in 308 seconds (5:08 minutes    )
  end

  #https://chessprogramming.wikispaces.com/Perft+Results
  example "Kiwipete error at level 3" do
    b = Board.parse_fen(KIWIPETE)
    Perft.new(b).perft(1).should eq(           48)
    Perft.new(b).perft(2).should eq(        2_039)
    Perft.new(b).perft(3).should eq(       97_862)
    # Perft.new(b).perft(4).should eq(    4_085_603)
    # Perft.new(b).perft(5).should eq(  193_690_690)
    # Perft.new(b).perft(6).should eq(8_031_647_685)
  end
end
