require "./../spec_helper"

ASCII_1 = "
  rnbqkbnr   # a comment
  pppppppp
  ........
  ........
  ........
  ........
  PPPPPPPP
  RNBQKBNR
  w KQkq - 0 1
  "

  ASCII_2 = "
    rnbqkbnr
    p..ppppp
    .pp.....
    ........
    ........
    ........
    PPPPPPPP
    RNBQKBNR
    w KQkq a3 0 1
    "

describe "ASCII parsing" do
  it "sets the 32 pieces" do
    b = Board.parse_ascii(ASCII_1)

    b.pieces(A2..H2).should eq([WP, WP, WP, WP, WP, WP, WP, WP])
    b.piece(A1).should eq(WR)
    b.piece(B1).should eq(WN)
    b.piece(C1).should eq(WB)
    b.piece(D1).should eq(WQ)
    b.piece(E1).should eq(WK)
    b.piece(F1).should eq(WB)
    b.piece(G1).should eq(WN)
    b.piece(H1).should eq(WR)

    b.pieces(A7..H7).should eq([BP, BP, BP, BP, BP, BP, BP, BP])
    b.piece(A8).should eq(BR)
    b.piece(B8).should eq(BN)
    b.piece(C8).should eq(BB)
    b.piece(D8).should eq(BQ)
    b.piece(E8).should eq(BK)
    b.piece(F8).should eq(BB)
    b.piece(G8).should eq(BN)
    b.piece(H8).should eq(BR)
  end
  it "sets the en-passant info" do
    b = Board.parse_ascii(ASCII_1)
    b.en_pas_square.should eq(NO_SQ)
    b = Board.parse_fen(FEN_2)
    b.en_pas_square.should eq(E3)
  end

  it "sets the side of the player" do
    b = Board.parse_ascii(ASCII_1)
    b.side.should eq(WHITE)
    b = Board.parse_fen(FEN_2)
    b.side.should eq(BLACK)
  end

  it "sets the castling permissions" do
    b = Board.parse_ascii(ASCII_1)
    b.castle_perms.should eq(WQCA + WKCA + BQCA + BKCA)
    b = Board.parse_fen(FEN_2)
    b.castle_perms.should eq(BKCA)
  end

  it "updates the pawns bitboards" do
    b = Board.parse_ascii(ASCII_1)
    b.pawns[WHITE].value.should eq(0b11111111_u64 << 8)
    b.pawns[BLACK].value.should eq(0b11111111_u64 << 48)
    b.pawns[BOTH ].value.should eq((0b11111111_u64 << 8) + (0b11111111_u64 << 48))
  end

  it "updates the hash key" do
    k1  = Board.parse_fen(Chess::FEN_START_POSITION).hash_key
    k1b = Board.parse_fen(Chess::FEN_START_POSITION).hash_key
    k2  = Board.parse_fen(FEN_2                    ).hash_key
    k1.should     eq(k1b)
    k1.should_not eq(k2)
  end

  # TODO halfmoveclock  # number of halfmoves since .. (for 50-plies rule)
  # TODO moves          # 1
  # it "updates the bitboard"
  # it "updates the hash key" : a, b, c==a
end
