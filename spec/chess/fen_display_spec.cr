require "./../spec_helper"

describe "FEN display" do
  it "produces the same FEN that the one that generated the board" do
    source_fen = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 2"
    b = Board.parse_fen(source_fen)
    Board::Converter.to_fen(b).should eq(source_fen)
  end
end
