require "./../../spec_helper"

describe Chess::Move::Parser, "#parse_move" do

  it "returns NO_MOVE if the move is not in the list of valid moves" do
    valid_moves = [Move.assemble(A2, A4)]
    Chess::Move::Parser.parse("g2g4", valid_moves).should eq(Move::NOMOVE)
  end

  example "a quiet move" do
    m1, m2  = Move.assemble(A2, A4), Move.assemble(A2, A3)
    valid_moves = [m1, m2]
    Chess::Move::Parser.parse("a2a4",  valid_moves).should eq(m1)
    Chess::Move::Parser.parse("a2a4Q", valid_moves).should eq(Move::NOMOVE)
  end

  example "a promotion" do
    m1, m2  = Move.assemble(A2, A4), Move.assemble(A7, A8, EMPTY, WQ)
    valid_moves = [m1, m2]
    Chess::Move::Parser.parse("a7a8Q", valid_moves).should eq(m2)
    Chess::Move::Parser.parse("a7a8q", valid_moves).should eq(Move::NOMOVE)
    Chess::Move::Parser.parse("a7a8",  valid_moves).should eq(Move::NOMOVE)
  end
end
