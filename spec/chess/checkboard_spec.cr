require "./../spec_helper"

describe "verify_board(b)" do
  it "checks the 3 bitboards" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      Debug.verify_board(b, true).should be_truthy
      b.pawns[WHITE].set_bit(0)
      Debug.verify_board(b).should be_falsey
      b.pawns[WHITE].clear_bit(0)
      Debug.verify_board(b).should be_truthy
    end

    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.pawns[BLACK].set_bit(0)
      Debug.verify_board(b).should be_falsey
      b.pawns[BLACK].clear_bit(0)
      Debug.verify_board(b).should be_truthy
    end

    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.pawns[BOTH].set_bit(0)
      Debug.verify_board(b).should be_falsey
      b.pawns[BOTH].clear_bit(0)
      Debug.verify_board(b).should be_truthy
    end
  end

  it "checks the pieces lists" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.set_piece A1,  WK
      Debug.verify_board(b).should be_falsey
    end
  end

  it "checks the pieces counts" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.piece_num[WP]+= 1
      Debug.verify_board(b).should be_falsey
    end
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.big_pieces[WHITE]+= 1
      Debug.verify_board(b).should be_falsey
    end
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.min_pieces[WHITE]+= 1
      Debug.verify_board(b).should be_falsey
    end
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.maj_pieces[WHITE]+= 1
      Debug.verify_board(b).should be_falsey
    end
  end

  it "checks the material value" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.material[WHITE] += 100
      Debug.verify_board(b).should be_falsey
    end
  end

  it "checks #side" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.side = BOTH
      Debug.verify_board(b).should be_falsey
    end
  end

  it "checks #en_pas_square" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.side = WHITE
      b.en_pas_square = A3
      Debug.verify_board(b).should be_falsey
      b.en_pas_square = A6
      Debug.verify_board(b).should be_truthy
    end
  end

  it "checks #king_squares" do
    Board.parse_fen(FEN_START_POSITION).tap do |b|
      b.king_squares[WHITE] = A1
      Debug.verify_board(b).should be_falsey
      b.king_squares[WHITE] = E1
      Debug.verify_board(b).should be_truthy
    end
  end
end
