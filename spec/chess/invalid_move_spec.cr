require "./../spec_helper"

describe "invalid moves detection (WIP)" do
  it "detects invalid usage of MFLAGPS (when in DEBUG_MODE)" do
    DEBUG_MODE.should be_truthy

    b = Board.parse_fen(Chess::FEN_START_POSITION)
    MoveMaker.new(b).make_move Move.assemble(A2, A3)
    Move.assemble(A2, A3, EMPTY, EMPTY)
    expect_raises(Chess::InvalidMoveError) do
      MoveMaker.new(b).make_move Move.assemble(A2, A3, EMPTY, EMPTY, Move::MFLAGPS)
    end
  end

  it "detects missing MFLAGPS" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    expect_raises(Chess::InvalidMoveError) do
      MoveMaker.new(b).make_move Move.assemble(A2, A4)
    end
    expect_raises(Chess::InvalidMoveError) do
      MoveMaker.new(b).make_move Move.assemble(A7, A5)
    end
  end

  # example "#capture? detects capture moves" do
  #   mv = Move.assemble(A2, B3)
  #   fail "flag error" if mv.capture?
  #   mv = Move.assemble(A2, B3, WP)
  #   fail "flag error" unless mv.capture?
  #   mv = Move.assemble(A2, B3, BQ)
  #   fail "flag error" unless mv.capture?
  # end
  #
  # example "#promotion? detects promotion moves" do
  #   mv = Move.assemble(A2, B3)
  #   fail "flag error" if mv.promotion?
  #   mv = Move.assemble(A2, B3, EMPTY, WP)
  #   fail "flag error" unless mv.promotion?
  #   mv = Move.assemble(A2, B3, EMPTY, BQ)
  #   fail "flag error" unless mv.promotion?
  # end
  #
  # example "#pawn_start? detects pawn starts" do
  #   mv = Move.assemble(A2, B3)
  #   fail "flag error" if mv.pawn_start?
  #   mv = Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS)
  #   fail "flag error" unless mv.pawn_start?
  # end
  #
  # describe "#to_s" do
  #   example "a quiet move" do
  #     Move.assemble(A2, B3).to_s.should eq("a2b3")
  #   end
  #
  #   example "a pawn start move" do
  #     Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS).to_s.should eq("a2a4/ps")
  #   end
  #
  #   example "a capture move" do
  #     Move.assemble(A2, B3, BQ).to_s.should eq("a2b3/cap:q")
  #   end
  #
  #   example "a promotion move" do
  #     Move.assemble(A2, B3, EMPTY, BQ).to_s.should eq("a2b3/->q")
  #   end
  #
  #   example "a castling move" do
  #     Move.assemble(E1, G1, EMPTY, EMPTY, Move::MFLAGCA).to_s.should eq("e1g1/cas")
  #   end
  # end
end
