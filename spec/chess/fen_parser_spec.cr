require "./../spec_helper"

FEN_2 = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b k e3 3 5"

describe "FEN parsing" do
  example "the default position" do
    Chess::FEN_START_POSITION.should eq("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
  end

  it "sets the 32 pieces" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)

    b.pieces(A2..H2).should eq([WP, WP, WP, WP, WP, WP, WP, WP])
    b.piece(A1).should eq(WR)
    b.piece(B1).should eq(WN)
    b.piece(C1).should eq(WB)
    b.piece(D1).should eq(WQ)
    b.piece(E1).should eq(WK)
    b.piece(F1).should eq(WB)
    b.piece(G1).should eq(WN)
    b.piece(H1).should eq(WR)

    b.pieces(A7..H7).should eq([BP, BP, BP, BP, BP, BP, BP, BP])
    b.piece(A8).should eq(BR)
    b.piece(B8).should eq(BN)
    b.piece(C8).should eq(BB)
    b.piece(D8).should eq(BQ)
    b.piece(E8).should eq(BK)
    b.piece(F8).should eq(BB)
    b.piece(G8).should eq(BN)
    b.piece(H8).should eq(BR)
  end

  it "sets the en-passant info" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    b.en_pas_square.should eq(NO_SQ)
    b = Board.parse_fen(FEN_2)
    b.en_pas_square.should eq(E3)
  end

  it "sets the side of the player" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    b.side.should eq(WHITE)
    b = Board.parse_fen(FEN_2)
    b.side.should eq(BLACK)
  end

  it "sets the castling permissions" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    b.castle_perms.should eq(WQCA + WKCA + BQCA + BKCA)
    b = Board.parse_fen(FEN_2)
    b.castle_perms.should eq(BKCA)
  end

  it "sets the castling permissions to 0 when there are none" do
    fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 0 1"
    b = Board.parse_fen(fen)
    b.castle_perms.should eq(0)
    b = Board.parse_fen(FEN_2)
    b.castle_perms.should eq(BKCA)
  end

  it "updates the pawns bitboards" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    b.pawns[WHITE].value.should eq(0b11111111_u64 << 8)
    b.pawns[BLACK].value.should eq(0b11111111_u64 << 48)
    b.pawns[BOTH ].value.should eq((0b11111111_u64 << 8) + (0b11111111_u64 << 48))
  end

  it "updates the hash key" do
    k1  = Board.parse_fen(Chess::FEN_START_POSITION).hash_key
    k1b = Board.parse_fen(Chess::FEN_START_POSITION).hash_key
    k2  = Board.parse_fen(FEN_2                    ).hash_key
    k1.should     eq(k1b)
    k1.should_not eq(k2)
  end

  it "updates the list of materials" do
    Board.parse_fen(Chess::FEN_START_POSITION).material.should eq([54200, 54200])
  end

  it "sets the fifty_move and moves counters" do
    b = Board.parse_fen(FEN_2)
    b.fifty_move.should eq(3)
    #b.full moves.should eq(5)
  end

  describe "the plies count" do
    example "when it's whites turn" do
      fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 2"
      b = Board.parse_fen(fen)
      b.his_ply     .should eq(2)
      b.moves_count .should eq(2)
    end

    example "when it's black turn" do
      fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 2"
      b = Board.parse_fen(fen)
      b.his_ply     .should eq(3)
      b.moves_count .should eq(2)
    end
  end
end
