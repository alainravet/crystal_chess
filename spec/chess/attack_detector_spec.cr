require "./../spec_helper"

#FEN_2 = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b k e3 3 5"

def attacked_squares_algebraic(b : Board, attacker_side : PieceColour)
  sq120s = attacked_squares(b, attacker_side)
  sq120s.map{|sq| algebraic_for(sq) }.sort
end

def attacked_squares(b : Board, attacker_side : PieceColour)
  res = [] of Cell120Coord
  (RANK_1..RANK_8).each do |rank|
    (FILE_A..FILE_H).each do |file|
      sq120 = sq120(file, rank)
      if AttackDetector.square_attacked_by?(b, sq120, attacker_side)
        res << sq120
      end
    end
  end
  res
end


describe Chess::AttackDetector, "#square_attacked?" do

  it "detects attack by pawns" do
    fen = "p7/1p6/8/8/8/8/1P6/8 w KQkq - 0 1"
    # 8 p.......
    # 7 .p......
    # 6 ........
    # 5 ........
    # 4 ........
    # 3 ........
    # 2 .P......
    # 1 ........
    #   abcdefgh
    b = Board.parse_fen(fen)
    attacked_squares_algebraic(b, WHITE).should eq(["a3", "c3"])
    attacked_squares_algebraic(b, BLACK).should eq(["a6", "b7", "c6"])
  end

  it "detects attack by knights" do
    fen = "1n6/8/8/8/8/8/8/1N6 w KQkq - 0 1"
    b = Board.parse_fen(fen)

    attacked_squares_algebraic(b, WHITE).should eq(["a3", "c3", "d2"])
    attacked_squares_algebraic(b, BLACK).should eq(["a6", "c6", "d7"])
  end

  it "detects attack by kings" do
    fen = "1k6/8/8/8/8/8/8/1K6 w KQkq - 0 1"
    b = Board.parse_fen(fen)

    attacked_squares_algebraic(b, WHITE).should eq(["a1", "a2", "b2", "c1", "c2"])
    attacked_squares_algebraic(b, BLACK).should eq(["a7", "a8", "b7", "c7", "c8"])
  end

  it "detects attack by rooks" do
    fen = "8/3r4/8/8/8/8/3R4/8 w KQkq - 0 1"
    # 8 ........
    # 7 ...r....
    # 6 ...X....
    # 5 ...X....
    # 4 ...X....
    # 3 ...X....
    # 2 XXX?XXXX
    # 1 ...X....
    #   abcdefgh

["a2", "b2", "c2", "d1", "d3", "d4", "d5", "d6", "d7", "e2", "f2", "g2", "h2"]
    b = Board.parse_fen(fen)

    attacked_squares_algebraic(b, WHITE).should eq(["a2", "b2", "c2", "d1", "d3", "d4", "d5", "d6", "d7", "e2", "f2", "g2", "h2"])
    # 8 ...X....
    # 7 XXX?XXXX
    # 6 ...X....
    # 5 ...X....
    # 4 ...X....
    # 3 ...X....
    # 2 ...R....
    # 1 ........
    #   abcdefgh
    attacked_squares_algebraic(b, BLACK).should eq(["a7", "b7", "c7", "d2", "d3", "d4", "d5", "d6", "d8", "e7", "f7", "g7", "h7"])
  end

  it "detects attack by bishops" do
    fen = "8/8/1b6/8/8/8/5B2/8 w KQkq - 0 1"
    # 8 ........
    # 7 ........
    # 6 .b......
    # 5 ........
    # 4 ........
    # 3 ........
    # 2 .....B..
    # 1 ........
    #   abcdefgh
    b = Board.parse_fen(fen)
    attacked_squares_algebraic(b, WHITE).should eq(["b6", "c5", "d4", "e1", "e3", "g1", "g3", "h4"])
    # 8 ...X....
    # 7 X.X.....
    # 6 .?......
    # 5 X.X.....
    # 4 ...X....
    # 3 ....X...
    # 2 .....B..
    # 1 ........
    #   abcdefgh
    attacked_squares_algebraic(b, BLACK).should eq(["a5", "a7", "c5", "c7", "d4", "d8", "e3", "f2"])
  end
  it "detects attack by rooks" do
    fen = "8/3r4/8/8/8/8/3R4/8 w KQkq - 0 1"
    # 8 ........
    # 7 ...r....
    # 6 ...X....
    # 5 ...X....
    # 4 ...X....
    # 3 ...X....
    # 2 XXX?XXXX
    # 1 ...X....
    #   abcdefgh

["a2", "b2", "c2", "d1", "d3", "d4", "d5", "d6", "d7", "e2", "f2", "g2", "h2"]
    b = Board.parse_fen(fen)

    attacked_squares_algebraic(b, WHITE).should eq(["a2", "b2", "c2", "d1", "d3", "d4", "d5", "d6", "d7", "e2", "f2", "g2", "h2"])
    # 8 ...X....
    # 7 XXX?XXXX
    # 6 ...X....
    # 5 ...X....
    # 4 ...X....
    # 3 ...X....
    # 2 ...R....
    # 1 ........
    #   abcdefgh
    attacked_squares_algebraic(b, BLACK).should eq(["a7", "b7", "c7", "d2", "d3", "d4", "d5", "d6", "d8", "e7", "f7", "g7", "h7"])
  end

  it "detects attack by queens" do
    fen = "8/8/1q6/8/8/8/5Q2/8 w KQkq - 0 1"
    # 8 ........
    # 7 ........
    # 6 .q......
    # 5 ........
    # 4 ........
    # 3 ........
    # 2 .....Q..
    # 1 ........
    #   abcdefgh
    b = Board.parse_fen(fen)
    #["a2", "b2", "b6", "c2", "c5", "d2", "d4", "e1", "e2", "e3", "f1", "f3", "f4", "f5", "f6", "f7", "f8", "g1", "g2", "g3", "h2", "h4"]
    # 8 .....X..
    # 7 .....X..
    # 6 .q...X..
    # 5 ..X..X..
    # 4 ...X.X.X
    # 3 ....XXX.
    # 2 XXXXX?XX
    # 1 ....XXX.
    #   abcdefgh
    attacked_squares_algebraic(b, WHITE).should eq(["a2", "b2", "b6", "c2", "c5", "d2", "d4", "e1", "e2", "e3", "f1", "f3", "f4", "f5", "f6", "f7", "f8", "g1", "g2", "g3", "h2", "h4"])
    attacked_squares_algebraic(b, BLACK).should eq(["a5", "a6", "a7", "b1", "b2", "b3", "b4", "b5", "b7", "b8", "c5", "c6", "c7", "d4", "d6", "d8", "e3", "e6", "f2", "f6", "g6", "h6"])
  end
end
