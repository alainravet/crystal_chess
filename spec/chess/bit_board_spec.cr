require "./../spec_helper"

describe Chess::BitBoard do

  describe "#value" do
    it "is 0 by default" do
      Chess::BitBoard.new().value.should eq(0_u64)
    end
    it "is an unsigned 64 bit init" do
      Chess::BitBoard.new(17_u64).value.should eq(17_u64)
    end
  end

  example "#count_bits returns the number of bits set to 1" do
    bm = Chess::BitBoard.new(0b1011100_u64)

    bm.count_bits.should eq(4)
  end

  example "#set_bit(i) sets the 0-indexed ith bit" do
    bm = Chess::BitBoard.new(0b101_u64)

    bm.set_bit(5)
    bm.value.should eq(0b100101)
  end

  example "#clear_bit(i) clears the 0-indexed ith bit" do
    bm = Chess::BitBoard.new(0b100101_u64)

    bm.clear_bit(5)
    bm.value.should eq(0b101)
  end

  example "#popbit returns the index of the LSB and clears it" do
    bm = Chess::BitBoard.new(0b1011100_u64)

    idx = bm.pop_bit

    idx.should eq(2)
    bm.value.should eq(0b1011000)
  end
end
