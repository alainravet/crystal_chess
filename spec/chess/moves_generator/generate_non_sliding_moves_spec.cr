require "./../../spec_helper"

describe MoveGenerator, "non-sliding moves" do
  example "a knight" do
    white_moves_a("
    ........   #     ........
    ........   #     ........
    ........   #     ........
    ........   #     ........
    ........   #     *.*..... a4  c4
    ........   #     ...*....       d3
    .N......   #     .N......
    ...p....   #     ...p....       /d1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq("b2a4, b2c4, b2d1/cap:p, b2d3")
  end

  example "a king " do
    white_moves_a("
    ........   #     ........
    ........   #     ........
    ........   #     ........
    ........   #     ........
    ........   #     ........
    pp......   #     pp*..... /a3/b3 c3
    .K......   #     *K*.....  a2    c2
    .pp.....   #     *pp.....  a1/b1/c1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq("b2a1, b2a2, b2a3/cap:p, b2b1/cap:p, b2b3/cap:p, b2c1/cap:p, b2c2, b2c3")
  end
end
