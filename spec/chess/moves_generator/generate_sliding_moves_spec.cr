require "./../../spec_helper"

describe MoveGenerator, "sliding moves" do
  example "a bishop with no obstacle" do
    white_moves_a("
    ........   #     .......*                h8
    ........   #     ......*.              g7
    ........   #     .....*..            f6
    ........   #     *...*...  a5      e5
    ........   #     .*.*....    b4  d4
    ..B.....   #     ..B.....
    ........   #     .*.*....    b2  d2
    ........   #     *...*...  a1      e1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq("c3a1, c3a5, c3b2, c3b4, c3d2, c3d4, c3e1, c3e5, c3f6, c3g7, c3h8")
  end

  example "a bishop with obstacles to capture" do
    white_moves_a("
    ........   #     ........
    ........   #     ........
    ........   #     ........
    ....p...   #     ....p...          /e5
    .p......   #     .p.*....   /b4  d4
    ..B.....   #     ..B.....
    ........   #     .*.*....    b2  d2
    p...p...   #     p...p... /a1      /e1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq("c3a1/cap:p, c3b2, c3b4/cap:p, c3d2, c3d4, c3e1/cap:p, c3e5/cap:p")
  end

  example "a rook" do
    white_moves_a("
    ........   #     ..*.....      c8
    ........   #     ..*.....      c7
    ........   #     ..*.....      c6
    ........   #     ..*.....      c5
    ........   #     ..*.....      c4
    ..R.....   #     **R*****  a3b3  d3e3f3g3h3
    ........   #     ..*.....      c2
    ........   #     ..*.....      c1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq("c3a3, c3b3, c3c1, c3c2, c3c4, c3c5, c3c6, c3c7, c3c8, c3d3, c3e3, c3f3, c3g3, c3h3")
  end

  example "a queen with obstacles to capture" do
    white_moves_a("
    ........   #     ........
    ........   #     ........
    .....b..   #     .....b..                 /f6
    ..r.....   #     ..r.*...        /c5    e5
    .b......   #     .b**....     /b4 c4 d4
    r.Q..r..   #     r*Q**r..  /a3 b3    d3 e3/f3
    .br.....   #     .br*....     /b2/c2 d2
    ....b...   #     ....b...              /e1
    w - - 0 1
    ")
      .map(&.to_s(false)).sort.join(", ")
      .should eq(
          "c3a3/cap:r, c3b2/cap:b, c3b3, c3b4/cap:b, c3c2/cap:r, c3c4, "+
          "c3c5/cap:r, c3d2, c3d3, c3d4, c3e1/cap:b, c3e3, c3e5, c3f3/cap:r, "+
          "c3f6/cap:b"
      )
  end
end
