require "./../spec_helper"

describe Chess::Board do

  describe "in a virgin Board" do
    example "the history is empty" do
      Board.new.tap do |b|
        b.history.size.should eq(2048)
        b.his_ply.should eq(0)
      end
      Board.parse_fen(FEN_START_POSITION).tap do |b|
        b.history.size.should eq(2048)
        b.his_ply.should eq(0)
      end
    end
  end

  describe "#generate_hash_key" do
    it "uses the pieces on the board" do
      b = Chess::Board.new
      k_before = b.generate_hashkey
      b.set_piece A1,  WR
      k_after = b.generate_hashkey
      k_after.should_not eq(k_before)
    end

    example "undoing a piece change restores the previous hash key value" do
      b = Chess::Board.new
      k_before = b.generate_hashkey

      # DO
      b.set_piece A1,  WR
      k_after = b.generate_hashkey
      # UNDO
      b.set_piece A1,  EMPTY
      b.generate_hashkey.should eq(k_before)
    end

    #TODO : test enPassant's impact on hash key
    #TODO : test enPassant's impact on hash key
  end

  describe "#update_lists_materials" do
    it "updates the 3 pawns bitboards" do
      b = create_board_1_and_update_lists_materials

      w_v = (1_u64<<sq64(A2)) + (1_u64<<sq64(B2)) + (1_u64<<sq64(C2)) + (1_u64<<sq64(D2)) +
            (1_u64<<sq64(E2)) + (1_u64<<sq64(F2)) + (1_u64<<sq64(G2)) + (1_u64<<sq64(H2))
      b_v = 1_u64<<sq64(A7)
      b.pawns[WHITE].value.should eq(w_v)
      b.pawns[BLACK].value.should eq(b_v)
      b.pawns[BOTH ].value.should eq(w_v + b_v)
    end

    it "updates the king squares caches" do
      b = create_board_1_and_update_lists_materials
      b.king_squares[WHITE].should eq(E1)
      b.king_squares[BLACK].should eq(NO_SQ)
    end

    it "updates typed-pieces caches" do
      b = create_board_1_and_update_lists_materials

      b.big_pieces[WHITE].should eq(8)
      b.maj_pieces[WHITE].should eq(4) # R Q K R
      b.min_pieces[WHITE].should eq(4) # N B B N
      # 100,   325,   325,   550,   1000,  50_000,
      # w_mat = 8*100 + 2*325 + 2*550 + 1000 + 50_000
      # b.material[WHITE].should eq(w_mat)

      b.big_pieces[BLACK].should eq(0)
      b.min_pieces[BLACK].should eq(0)
      b.maj_pieces[BLACK].should eq(0)
      b_mat = 100
      b.material[BLACK].should eq(b_mat)
    end

    it "updates the pieces list and counters" do
      b = create_board_1_and_update_lists_materials

      b.piece_list[WR].should eq( ([A1, H1] + NO_SQS).first(10) )
      b.piece_list[WN].should eq( ([B1, G1] + NO_SQS).first(10) )
      b.piece_list[WB].should eq( ([C1, F1] + NO_SQS).first(10) )
      b.piece_list[WQ].should eq( ([D1] + NO_SQS).first(10) )
      b.piece_list[WK].should eq( ([E1] + NO_SQS).first(10) )
      b.piece_list[WP].should eq( ([A2, B2, C2, D2, E2, F2, G2, H2] + NO_SQS).first(10) )

      b.piece_num[WR].should eq(2)
      b.piece_num[WN].should eq(2)
      b.piece_num[WB].should eq(2)
      b.piece_num[WQ].should eq(1)
      b.piece_num[WK].should eq(1)
      b.piece_num[WP].should eq(8)
    end
  end
end
