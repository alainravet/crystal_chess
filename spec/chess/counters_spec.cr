
require "./../spec_helper"
#
describe "the various counters" do
  # source: http://www.thechessdrum.net/PGN_Reference.txt

  example "they are initialized when a board if created from FEN_START_POSITION" do
    # Here's the FEN for the starting position:
    #   rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    b.his_ply     .should eq(0)
    b.moves_count .should eq(1)
    b.fifty_move  .should eq(0)
    b.ply         .should eq(0)
  end

  example "they are updated by #make_move and #take_move" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)

    # And after the move 1. e4:
    MoveMaker.new(b).make_move Move.assemble(E2, E4, EMPTY, EMPTY, Move::MFLAGPS)
    # rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1

    b.fifty_move  .should eq(0)
    b.moves_count .should eq(1)
    b.his_ply     .should eq(1)
    b.ply         .should eq(1)

    #
    # And then after 1. ... c5:
    # rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2
    MoveMaker.new(b).make_move Move.assemble(C7, C5, EMPTY, EMPTY, Move::MFLAGPS)

    b.fifty_move  .should eq(0)
    b.moves_count .should eq(2)
    b.his_ply     .should eq(2)
    b.ply         .should eq(2)

    #
    # And then after 2. Nf3:
    # rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2
    MoveMaker.new(b).make_move Move.assemble(G1, F3)
    b.fifty_move  .should eq(1)
    b.moves_count .should eq(2)
    b.his_ply     .should eq(3)
    b.ply         .should eq(3)

    MoveMaker.new(b).take_move
    b.fifty_move  .should eq(0)
    b.moves_count .should eq(2)
    b.his_ply     .should eq(2)
    b.ply         .should eq(2)

    MoveMaker.new(b).take_move
    b.fifty_move  .should eq(0)
    b.moves_count .should eq(1)
    b.his_ply     .should eq(1)
    b.ply         .should eq(1)

    MoveMaker.new(b).take_move
    b.his_ply     .should eq(0)
    b.moves_count .should eq(1)
    b.fifty_move  .should eq(0)
    b.ply         .should eq(0)
  end
end
