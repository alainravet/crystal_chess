require "./../spec_helper"

describe Chess::Board, "by default" do
  describe "#pieces" do
    example "the 64 real cells are EMPTY" do
      b = Chess::Board.new
      (RANK_1..RANK_8).each do |rank|
        (FILE_A..FILE_H).each do |file|
          sq_120 = sq120(file, rank)
          b.piece(sq_120).should eq(Piece::EMPTY)
        end
      end
    end

    example "the extra cells are OFFBOARD" do
      b = Chess::Board.new
      extra_rows = [0, 1, 10, 11]
      extra_rows.each do |y|
        (0...Board::Grid::CELLS_PER_ROW).each do |x|
          idx = x + y*Board::Grid::CELLS_PER_ROW
          b.piece(idx).should eq(Piece::OFFBOARD)
        end
      end
      extra_cols = [0, 9]
      extra_cols.each do |x|
        (0...Board::Grid::NOF_ROWS).each do |y|
          idx = x + y*Board::Grid::CELLS_PER_ROW
          b.piece(idx).should eq(Piece::OFFBOARD)
        end
      end
    end
  end

  describe "#pawns" do
    example "the 3 bitboards are empty" do
      b = Chess::Board.new
      b.pawns[WHITE].value.should eq(Chess::BitBoard::EMPTY)
      b.pawns[BLACK].value.should eq(0_u64)
      b.pawns[BOTH ].value.should eq(0_u64)
    end
  end

  example "the inner properties..." do
    b = Chess::Board.new
    b.king_squares[WHITE] .should eq(NO_SQ)
    b.king_squares[BLACK] .should eq(NO_SQ)
    b.side                .should eq(BOTH)
    b.en_pas_square       .should eq(NO_SQ)
    b.fifty_move          .should eq(0)
    b.ply                 .should eq(0)
    b.his_ply             .should eq(0)
    b.castle_perms        .should eq(0b0000)
    b.hash_key            .should eq(0_u64)
  end

  describe "#pieces_list" do
    example "the squares where to find pieces are all nil" do
      b = Chess::Board.new
      (0..12).each do |piece|
        piece_squares = b.piece_list[piece]
        piece_squares.all?{|sq120| sq120 == NO_SQ}.should eq(true)
        b.piece_num[piece].should eq(0)
      end
    end
  end
end
