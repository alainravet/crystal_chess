require "./../../spec_helper"

describe Chess::Search::Evaluator, "#evaluate" do

    example "the score of the starting position is 0" do
      board = Board.parse_fen(FEN_START_POSITION)
      score = Chess::Search::Evaluator.new(board).evaluate
      score.should eq(0)
    end

    example "the score of KIWIPETE is 90" do
      board = Board.parse_fen(KIWIPETE)
      score = Chess::Search::Evaluator.new(board).evaluate
      score.should eq(90)
    end

    example "another score explained" do
      board = Board.parse_ascii("
        r.bqkbn.
        ppppppp.
        ..n.Q...    # BAD ALGO: WQ ugly's position is not taken into account
        ...r....
        ........
        N...B...
        PPP.PPPP
        R...KBNR
        w KQkq - 10 10
        ")

      score = Chess::Search::Evaluator.new(board).evaluate
      score.should eq(0 +
        - (100 -10) + # loss : lost WP d2
        + (     25) + # gain : better position for WB  (-10->+15)
        + (     10) + # gain : better position for WN  (-10->0)

        + (100 +10) + # gain : 1 less BP h7
        - (     10) + # loss : better position for BR d5
        - (     20) + # loss : better position for BN  (-10->10)
        0
      )
    end
end
