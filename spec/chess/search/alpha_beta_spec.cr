require "./../../spec_helper"

def do_alpha_beta(board_or_fen, depth)
  board = board_or_fen.is_a?(Board) ?
    board_or_fen :
    Board.parse_fen(board_or_fen)
  search_info = Chess::Search::Info.new

  Chess::Search::Searcher.new(board, search_info).tap do |sr|
    sr.alpha_beta(-Int32::MAX, Int32::MAX, depth)
  end
  search_info
end


describe Chess::Search::Searcher, "#alpha_beta" do

  context "depth = 0" do
    it "returns the evaluation score of the current position
        and increments the nodes counter by 1
    " do
      board       = Board.parse_fen(KIWIPETE)
      search_info = Chess::Search::Info.new

      pete_score  = Chess::Search::Evaluator.new(board).evaluate
      Chess::Search::Searcher.new(board, search_info).tap do |sr|
        alpha = sr.alpha_beta(-Int32::MAX, Int32::MAX, depth=0)
        alpha.should eq(pete_score)
      end
      search_info.nodes.should eq(1)
    end
  end

  context "the starting position" do
#     rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 ;
#     D1 20
#     D2 400
#     D3 8902   ;
#     D4 197281 ; D5 4865609 ;D6 119060324

    it "visits all the 20 nodes at depth 1" do
      search_info = do_alpha_beta(FEN_START_POSITION, 1)
      search_info.nodes.should eq(1 + 20)
    end

    it "ignores 218 nodes at depth 2" do
      search_info = do_alpha_beta(FEN_START_POSITION, 2)
      search_info.nodes.should eq(1 + 20 + (400-218))
    end

    it "ignores 7301 nodes at depth 3" do
      search_info = do_alpha_beta(FEN_START_POSITION, 3)
      search_info.nodes.should eq(1 + (20 + 400 + 8902)-7301)
    end
  end

  context "a repeat position" do
    it "returns a score of 0 and does not visit other nodes" do
      b = Board.parse_fen(Chess::FEN_START_POSITION)
      MoveMaker.new(b).make_move  Move.assemble(B1, A3)
      MoveMaker.new(b).make_move  Move.assemble(B8, A6)
      MoveMaker.new(b).make_move  Move.assemble(A3, B1)
      MoveMaker.new(b).make_move  Move.assemble(A6, B8) # back to start post

      search_info = Chess::Search::Info.new
      Chess::Search::Searcher.new(b, search_info).tap do |sr|
        score = sr.alpha_beta(-Int32::MAX, Int32::MAX, depth=666)
        score.should eq(0)
      end
      search_info.nodes.should eq(1 + 0)
    end
  end

  context "depth 4" do
    it "suggests b1c3 e7e5 e2e4 d7d5" do
      board       = Board.parse_fen(FEN_START_POSITION)
      search_info = do_alpha_beta(board, 4)

      board.fill_pv_line_to_s(4).should eq("b1c3 e7e5 e2e4 d7d5")
      search_info.nodes.should eq(22735)
    end

    example "the ordering factor is 29%" do
      search_info = do_alpha_beta(FEN_START_POSITION, 4)
      search_info.ordering_pct.round(2).should eq(0.29)
    end
  end
end
