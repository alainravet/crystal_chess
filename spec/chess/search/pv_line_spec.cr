require "./../../spec_helper"

describe "#get_pv_line" do
  example "play a few moves, fill and show pv_array" do
    b = Board.parse_fen(Chess::FEN_START_POSITION)
    m1=Move.assemble(A2, A3)
    b.store_pv_move m1
    MoveMaker.new(b).make_move  m1

    m2=Move.assemble(A7, A6)
    b.store_pv_move m2
    MoveMaker.new(b).make_move m2

    m3=Move.assemble(A3, A4)
    b.store_pv_move m3
    MoveMaker.new(b).make_move m3

    m4=Move.assemble(A6, A5)
    b.store_pv_move m4
    MoveMaker.new(b).make_move  m4

    MoveMaker.new(b).take_move
    MoveMaker.new(b).take_move
    MoveMaker.new(b).take_move
    MoveMaker.new(b).take_move
    b.get_pv_line(4)

    # Note: this test can fail if a collision occurs in
    # the pv_table (solution: use a bigger table -> slower)
    b.pv_array.compact.should eq([m1, m2, m3, m4])
  end
end
