describe Chess::Search::Info do
  it "has properties" do
    si = Chess::Search::Info.new
    si.starttime
    si.stoptime
    si.depth
    si.depthset
    si.timeset
    si.movestogo
    si.infinite
    si.nodes      .should eq(0)
    si.quit
    si.stopped
    si.fh         .should eq(0)
    si.fhf        .should eq(0)
  end
end
