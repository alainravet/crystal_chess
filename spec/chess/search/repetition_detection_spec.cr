require "./../../spec_helper"

describe "#position_repetition?" do
  example "detect a repetition of the starting position" do
    b = Board.parse_ascii("
      .......k
      ........
      .......K
      ........
      ........
      ........
      ........
      N......n
      w - - 10 7
      ")

      MoveMaker.new(b).make_move  Move.assemble(A1, C2) # W
      MoveMaker.new(b).make_move  Move.assemble(H8, G8) # b
      MoveMaker.new(b).make_move  Move.assemble(H6, G6) # W
      MoveMaker.new(b).make_move  Move.assemble(H1, F2) # b
      MoveMaker.new(b).make_move  Move.assemble(C2, A1) # W
      MoveMaker.new(b).make_move  Move.assemble(F2, H1) # b
      b.repetition_detected?.should be_falsey

      MoveMaker.new(b).make_move  Move.assemble(G6, H6) # W
      MoveMaker.new(b).make_move  Move.assemble(G8, H8) # b
      b.repetition_detected?.should be_truthy
  end
end
