describe Chess::Search::PvTable do
  example "#get(key) returns nil if the key is unknown" do
    t = Search::PvTable.new(2*20)
    t.get(666_u64).should eq(nil)
  end

  example "Move are retrieved by their hashkeys property" do
    t  = Search::PvTable.new(2+2)

    hashkey = 12345_u64
    m = Move.new(17_u64)
    t.store hashkey, m

    t.get(hashkey).should eq(m)
  end

  describe "#get" do
    example "#it returns the last stored entry when a collision occurs" do
      t = Search::PvTable.new(1+2)  # only 1 slot -> always collision
      t.store 11111_u64, m1 = Move.new(11_u64)
      t.store 22222_u64, m2 = Move.new(22_u64)
      t.get(22222_u64).should eq(m2)
    end

    example "collisions erase older entries" do
      t = Search::PvTable.new(1+2)  # only 1 slot -> always collision
      key_1 = 11111_u64

      t.store key_1, m1=Move.new(11_u64)
      t.get(key_1).should eq(m1)

      t.store 22222_u64, Move.new(22_u64)
      t.get(key_1).should eq(nil)
    end
  end
end
