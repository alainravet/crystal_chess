require "./../../spec_helper"

describe MoveMaker, "#add_piece" do

  it "adds the piece to the board" do
    b = Board.parse_fen(AFT_FEN_A7)
    b.piece(A7).should eq(EMPTY)
    MoveMaker.new(b).add_piece(A7, BP)
    b.piece(A7).should eq(BP)

    b.pieces.should eq(Board.parse_fen(BEF_FEN_1).pieces)
  end

  it "updates the material value" do
    b = Board.parse_fen(AFT_FEN_A7)
    MoveMaker.new(b).add_piece(A7, BP)

    b.material.should eq(Board.parse_fen(BEF_FEN_1).material)
  end

  it "updates the pawns bitboards" do
    b = Board.parse_fen(AFT_FEN_A7)
    MoveMaker.new(b).add_piece(A7, BP)

    b.pawns.should eq(Board.parse_fen(BEF_FEN_1).pawns)
  end

  it "updates the pieces size caches" do
    Board.parse_fen(AFT_FEN_A8).tap do |b|
      aft_b = Board.parse_fen(BEF_FEN_1)
      MoveMaker.new(b).add_piece(A8, BR)

      b.big_pieces.should eq(aft_b.big_pieces)
      b.maj_pieces.should eq(aft_b.maj_pieces)
      b.min_pieces.should eq(aft_b.min_pieces)
    end
    Board.parse_fen(AFT_FEN_B8).tap do |b|
      aft_b = Board.parse_fen(BEF_FEN_1)
      MoveMaker.new(b).add_piece(B8, BN)

      b.big_pieces.should eq(aft_b.big_pieces)
      b.maj_pieces.should eq(aft_b.maj_pieces)
      b.min_pieces.should eq(aft_b.min_pieces)
    end
  end

  it "updates the pieces list" do
    Board.parse_fen(AFT_FEN_D7).tap do |b|
      aft_b = Board.parse_fen(BEF_FEN_1)
      MoveMaker.new(b).add_piece(D7, BP)

      b.piece_num .should eq(aft_b.piece_num)
      act = b    .piece_list_clean(BP).sort
      exp = aft_b.piece_list_clean(BP).sort
      act.should eq(exp)
    end
  end

  it "updates the board hash key" do
    Board.parse_fen(AFT_FEN_H8).tap do |b|
      MoveMaker.new(b).add_piece(H8, BR)
      b.hash_key.should eq(Board.parse_fen(BEF_FEN_1).hash_key)
    end
  end
end
