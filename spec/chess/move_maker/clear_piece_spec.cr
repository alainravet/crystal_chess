require "./../../spec_helper"

describe MoveMaker, "#clear_piece" do

  it "removes the piece from the board" do
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).clear_square(A7)

    b.pieces.should eq(Board.parse_fen(AFT_FEN_A7).pieces)
  end

  it "updates the material value" do
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).clear_square(A7)

    b.material.should eq(Board.parse_fen(AFT_FEN_A7).material)
  end

  it "updates the pawns bitboards" do
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).clear_square(A7)

    b.pawns.should eq(Board.parse_fen(AFT_FEN_A7).pawns)
  end

  it "updates the pieces size caches" do
    Board.parse_fen(BEF_FEN_1).tap do |b|
      aft_b = Board.parse_fen(AFT_FEN_A8)
      MoveMaker.new(b).clear_square(A8)

      b.big_pieces.should eq(aft_b.big_pieces)
      b.maj_pieces.should eq(aft_b.maj_pieces)
      b.min_pieces.should eq(aft_b.min_pieces)
    end
    Board.parse_fen(BEF_FEN_1).tap do |b|
      aft_b = Board.parse_fen(AFT_FEN_B8)
      MoveMaker.new(b).clear_square(B8)

      b.big_pieces.should eq(aft_b.big_pieces)
      b.maj_pieces.should eq(aft_b.maj_pieces)
      b.min_pieces.should eq(aft_b.min_pieces)
    end
  end

  it "updates the pieces list" do
    Board.parse_fen(BEF_FEN_1).tap do |b|
      aft_b = Board.parse_fen(AFT_FEN_D7)
      MoveMaker.new(b).clear_square(D7)

      b.piece_num .should eq(aft_b.piece_num)
      # success
      #  aft_b:       [81, 82, 83, 85, 86, 87, 88,      99, 99, 99]
      #      b:       [81, 82, 83, 88, 85, 86, 87,      88, 99, 99]
      # 7 pawns left   --------------------------
      # and order is not important
      piece = BP # removed from D7
      act = b    .piece_list_clean(piece).sort
      exp = aft_b.piece_list_clean(piece).sort
      act.should eq(exp)
    end
  end

  it "updates the board hash key" do
    Board.parse_fen(BEF_FEN_1).tap do |b|
      MoveMaker.new(b).clear_square(H8)
      b.hash_key.should eq(Board.parse_fen(AFT_FEN_H8).hash_key)
    end
  end
end
