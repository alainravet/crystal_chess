require "./../../spec_helper"
#
# BEF_MAK_MOVE      =   "rnbqkbnr/ppp2ppp/8/8/8/8/PPP2PPP/RNBQKBNR b KQkq - 10 7"
# AFT_MAK_MOV_B8_A6 =   "r1bqkbnr/ppp2ppp/n7/8/8/8/PPP2PPP/RNBQKBNR w KQkq - 11 7"
# AFT_CAP_WQ        =   "rnb1kbnr/ppp2ppp/8/8/8/8/PPP2PPP/RNBqKBNR b KQkq - 0 7"

describe MoveMaker, "#take_move" do

  example "undoing a capture move" do
    pos_1 = "
      rnbqkbnr
      ppp..ppp
      ........
      ........
      ........
      ........
      PPP..PPP
      RNBQKBNR
      b KQkq - 10 7
    "
    pos_2 = "
      rnb.kbnr
      ppp..ppp
      ........
      ........
      ........
      ........
      PPP..PPP
      RNBqKBNR
      w KQkq - 0 7
    "

    b = Board.parse_ascii(pos_1)
    MoveMaker.new(b).make_move Move.assemble(D8, D1, WQ)
    b.should eq(Board.parse_ascii(pos_2))
    MoveMaker.new(b).take_move
    b.should eq(Board.parse_ascii(pos_1))
  end

  example "undoing a promotion move" do
    pos_1 = "
      .......k
      ........
      ........
      ........
      ........
      ........
      p.......
      .......K
      b - - 0 11
      "
    pos_2 = "
      .......k
      ........
      ........
      ........
      ........
      ........
      ........
      q......K
      w - - 0 11
      "

      b = Board.parse_ascii(pos_1)
      MoveMaker.new(b).make_move Move.assemble(A2, A1, EMPTY, BQ)
      b.should eq(Board.parse_ascii(pos_2))
      MoveMaker.new(b).take_move
      b.should eq(Board.parse_ascii(pos_1))
  end

  example "undoing an en-passant move" do
    pos_1 = "
      .......k
      ........
      ........
      ........
      .Pp.....
      ........
      ........
      .......K
      b - b3 0 11
      "
    pos_2 = "
      .......k
      ........
      ........
      ........
      ........
      .p......
      ........
      .......K
      w - - 0 11
      "

    b = Board.parse_ascii(pos_1)
    m = Move.assemble(C4, B3, EMPTY, EMPTY, Move::MFLAGEP)
    MoveMaker.new(b).make_move m
    b.should eq(Board.parse_ascii(pos_2))
    MoveMaker.new(b).take_move
    b.should eq(Board.parse_ascii(pos_1))
  end

  example "undoing a castling move" do
    pos_1 = "
      r...k..r
      ........
      ........
      ........
      ........
      ........
      ........
      R...K..R
      b KQkq - 5 11
      "
    pos_2 = "
      r....rk.
      ........
      ........
      ........
      ........
      ........
      ........
      R...K..R
      w KQ - 6 11
      "

    b = Board.parse_ascii(pos_1)
    m = Move.assemble(E8, G8, EMPTY, EMPTY, Move::MFLAGCA)

    MoveMaker.new(b).make_move m
    b.should eq(Board.parse_ascii(pos_2))

    MoveMaker.new(b).take_move
    b.should eq(Board.parse_ascii(pos_1))
  end

  example "undoing a pawn move that created an en-passant" do
    pos_1 = "
      .......k
      ........
      ........
      ........
      ........
      ........
      P.......
      .......K
      w - - 5 11
      "
    pos_2 = "
      .......k
      ........
      ........
      ........
      P.......
      ........
      ........
      .......K
      b - a3 0 11
      "

      b = Board.parse_ascii(pos_1)
      m = Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS)

      MoveMaker.new(b).make_move m
      b.should eq(Board.parse_ascii(pos_2))

      MoveMaker.new(b).take_move
      b.should eq(Board.parse_ascii(pos_1))
  end
end
