require "./../../spec_helper"

BEF_MAK_MOVE      =   "rnbqkbnr/ppp2ppp/8/8/8/8/PPP2PPP/RNBQKBNR b KQkq - 10 7"
AFT_MAK_MOV_B8_A6 =   "r1bqkbnr/ppp2ppp/n7/8/8/8/PPP2PPP/RNBQKBNR w KQkq - 11 7"
AFT_CAP_WQ        =   "rnb1kbnr/ppp2ppp/8/8/8/8/PPP2PPP/RNBqKBNR b KQkq - 0 7"

describe MoveMaker, "#make_move" do
  context "a simple move" do
    it "moves the piece on the board" do
      b = Board.parse_fen(BEF_MAK_MOVE)
      MoveMaker.new(b).make_move Move.assemble(B8, A6)

      b.pieces.should eq(Board.parse_fen(AFT_MAK_MOV_B8_A6).pieces)
    end

    it "switches the side" do
      Board.parse_fen(BEF_MAK_MOVE).tap do |b|
        b.side.should eq(BLACK)
        MoveMaker.new(b).make_move Move.assemble(B8, A6)
        b.side.should eq(WHITE)
      end
    end

    it "updates the hash_key" do
      Board.parse_fen(BEF_MAK_MOVE).tap do |b|
        MoveMaker.new(b).make_move Move.assemble(B8, A6)
        b.hash_key.should eq(Board.parse_fen(AFT_MAK_MOV_B8_A6).hash_key)
      end
    end

    it "increments #fifty_move" do
      b = Board.parse_fen(BEF_MAK_MOVE)
      old_fifty_move = b.fifty_move
      MoveMaker.new(b).make_move Move.assemble(B8, A6)

      b.fifty_move.should eq(1+old_fifty_move)
    end

    it "updates board#king_square" do
      Board.parse_fen(BEF_MAK_MOVE).tap do |b|
        b.king_squares[BLACK].should eq(E8)
        MoveMaker.new(b).make_move Move.assemble(E8, E7)
        b.king_squares[BLACK].should eq(E7)
      end
    end

    it "returns false if the moved king is attacked, true otherwise" do
      Board.parse_fen(BEF_MAK_MOVE).tap do |b|
        res = MoveMaker.new(b).make_move Move.assemble(E8, E7)
        res.should be_truthy
      end
      Board.parse_fen(BEF_MAK_MOVE).tap do |b|
        res = MoveMaker.new(b).make_move Move.assemble(E8, D7)
        res.should be_falsey
      end
    end

    it "increments the board plies counter" do
      b = Board.parse_fen(FEN_START_POSITION)
      ply_bef = b.ply
      MoveMaker.new(b).make_move Move.assemble(B1, A3)
      b.ply.should eq(1+ply_bef)
    end

    example "the pre-move state and the move are appended to the history" do
      b = Board.parse_fen(FEN_START_POSITION)
      hk_bef, hp_bef, fm_bef = b.hash_key, b.his_ply, b.fifty_move

      m = Move.assemble(B1, A3)
      MoveMaker.new(b).make_move m

      b.his_ply     .should     eq(1+hp_bef)
      b.fifty_move  .should     eq(1+fm_bef)
      b.hash_key    .should_not eq(hk_bef)

      b.history[b.his_ply-1].tap do |prev_state|
        prev_state.should be_a(Undo)
        if prev_state # for the compiler
          prev_state.move           .should eq(m)
          prev_state.hash_key       .should eq(hk_bef)
          #prev_state.castle_perms.should eq(hk_bef)
          #prev_state.en_pas_square.should eq(hk_bef)
          prev_state.fifty_move     .should eq(fm_bef)
          prev_state.hash_key       .should eq(hk_bef)
        end
      end
    end
  end

  example "a capture move" do
    b = Board.parse_ascii("
      rnbqkbnr
      ppp..ppp
      ........
      ........
      ........
      ........
      PPP..PPP
      RNBQKBNR
      b KQkq - 10 7
      ")
    MoveMaker.new(b).make_move Move.assemble(D8, D1, WQ)
    aft = Board.parse_ascii("
      rnb.kbnr
      ppp..ppp
      ........
      ........
      ........
      ........
      PPP..PPP
      RNBqKBNR
      w KQkq - 0 7
      ")
    b.should eq(aft)
    b.piece_num[WQ].should eq(0)
  end

  example "a promotion move" do
    b = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      ........
      ........
      p.......
      .......K
      b - - 0 11
      ")
    m = Move.assemble(A2, A1, EMPTY, BQ)
    MoveMaker.new(b).make_move m
    aft = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      ........
      ........
      ........
      q......K
      w - - 0 11
      ")
    b.should eq(aft)
  end

  example "an en-passant move" do
    b = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      .Pp.....
      ........
      ........
      .......K
      b - b3 0 11
      ")
    m = Move.assemble(C4, B3, EMPTY, EMPTY, Move::MFLAGEP)
    MoveMaker.new(b).make_move m
    aft = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      ........
      .p......
      ........
      .......K
      w - - 0 11
      ")
    b.piece_list_clean(BP).should eq(aft.piece_list_clean(BP))
    b.piece_list_clean(WP).should eq(aft.piece_list_clean(WP))
    b.pieces.should eq(aft.pieces)
    b.should eq(aft)
  end

  example "a pawn move that creates an en-passant" do
    b = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      ........
      ........
      P.......
      .......K
      w - - 5 11
      ")
    m = Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS)
    MoveMaker.new(b).make_move m
    aft = Board.parse_ascii("
      .......k
      ........
      ........
      ........
      P.......
      ........
      ........
      .......K
      b - a3 0 11
      ")
    b.should eq(aft)
  end


  example "a castling move (C1)" do
    b = Board.parse_ascii("
      r...k..r
      ........
      ........
      ........
      ........
      ........
      ........
      R...K..R
      w KQkq - 5 11
      ")
    m = Move.assemble(E1, C1, EMPTY, EMPTY, Move::MFLAGCA)
    MoveMaker.new(b).make_move m
    aft = Board.parse_ascii("
      r...k..r
      ........
      ........
      ........
      ........
      ........
      ........
      ..KR...R
      b kq - 6 11
      ")
    b.should eq(aft)
  end


  example "a castling move (G8)" do
    b = Board.parse_ascii("
      r...k..r
      ........
      ........
      ........
      ........
      ........
      ........
      R...K..R
      b KQkq - 5 11
      ")
    m = Move.assemble(E8, G8, EMPTY, EMPTY, Move::MFLAGCA)
    MoveMaker.new(b).make_move m
    aft = Board.parse_ascii("
      r....rk.
      ........
      ........
      ........
      ........
      ........
      ........
      R...K..R
      w KQ - 6 11
      ")
    b.should eq(aft)
  end

  it "undoes automatically illegal moves" do
    pos_1 = "
      r...k..r
      ........
      ..R.....  # reminder : `move` is pseudo-legal
      ........  # => we don't check here is castling is
      ........  #    legal (is done by the moves generator)
      ........
      ........
      R...K..R
      b KQkq - 5 11
      "
    b = Board.parse_ascii(pos_1)
    m = Move.assemble(E8, C8, EMPTY, EMPTY, Move::MFLAGCA)
    res = MoveMaker.new(b).make_move m
    res.should eq(false)
    b.should eq(Board.parse_ascii(pos_1))
  end
end
