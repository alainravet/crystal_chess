require "./../../spec_helper"

describe MoveMaker, "#move_piece" do
  #  MoveMaker.new(b).move_piece(B8, A6)
  # BEF_FEN_1  =   "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  # AFT_MOV_B8_A6 =   "r1bqkbnr/pppppppp/n7/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  # AFT_MOV_A7_A6 =   "rnbqkbnr/1ppppppp/p7/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"


  it "moves the piece on the board" do
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).move_piece(B8, A6)

    b.pieces.should eq(Board.parse_fen(AFT_MOV_B8_A6).pieces)
  end

  it "does not change the material value" do
    Board.parse_fen(BEF_FEN_1).material.should eq(Board.parse_fen(AFT_MOV_B8_A6).material)
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).move_piece(B8, A6)

    b.material.should eq(Board.parse_fen(AFT_MOV_B8_A6).material)
  end

  it "updates the pawns bitboards" do
    b = Board.parse_fen(BEF_FEN_1)
    MoveMaker.new(b).move_piece(A7, A6)

    b.pawns.should eq(Board.parse_fen(AFT_MOV_A7_A6).pawns)
  end

  it "updates the pieces list" do
    Board.parse_fen(BEF_FEN_1).tap do |b|
      aft_b = Board.parse_fen(AFT_MOV_A7_A6)
      MoveMaker.new(b).move_piece(A7, A6)

      act = b    .piece_list_clean(BP).sort
      exp = aft_b.piece_list_clean(BP).sort
      act.should eq(exp)
    end
  end

  it "updates the board hash key" do
    Board.parse_fen(BEF_FEN_1).tap do |b|
      MoveMaker.new(b).move_piece(B8, A6)
      b.hash_key.should eq(Board.parse_fen(AFT_MOV_B8_A6).hash_key)
    end
  end
end
