require "./../spec_helper"

describe Chess::Board, "structure and mapping" do

  it "is made of 120 cells" do
    Chess::Board.new.pieces.size.should eq(120)
  end

  example "sq120 maps file/ranks to grid_120 indices " do
    sq120(FILE_A, RANK_1).should eq(21)
    sq120(FILE_H, RANK_1).should eq(28)
    sq120(FILE_H, RANK_8).should eq(98)
  end

  example "Board::Utils.sq64 maps 12x10 grid cells to 8x8 squares" do
    sq64(21).should eq( 0)
    sq64(28).should eq( 7)
    sq64(98).should eq(63)
    sq64(98).should eq(63)
  end

  example "Board::Utils.sq120 maps 8x8 squares to 12x10 grid cells" do
    sq120( 0).should eq(21)
    sq120( 7).should eq(28)
    sq120(63).should eq(98)
    sq120(63).should eq(98)
  end

  example "SQ_TO_R[i] tells what *rank* a cell belongs to" do
    SQ_TO_R[ 1].should eq(OFFBOARD)
    SQ_TO_R[21].should eq(RANK_1)
    SQ_TO_R[28].should eq(RANK_1)
    SQ_TO_R[31].should eq(RANK_2)
  end

  example "SQ_TO_F[i] tells what *file* a cell belongs to" do
    SQ_TO_F[ 1].should eq(OFFBOARD)
    SQ_TO_F[21].should eq(FILE_A)
    SQ_TO_F[28].should eq(FILE_H)
    SQ_TO_F[31].should eq(FILE_A)
  end


  example "all the 64 cells can be mapped 120 <-> 64" do
    (1..63).each do |sq64|
      sq120 = sq120(sq64)
      sq64(sq120).should eq(sq64)
    end
  end
end
