require "./../spec_helper"

describe Move do
  example "Move.assemble(x,x,x,x) assembles the elements and returns 1 value" do
    mv = Move.assemble(A2, B3, WN, EMPTY, Move::MFLAGCA)
    mv.should be_a(Move)
    mv.from .should eq(A2)
    mv.to   .should eq(B3)
    mv.captured_piece.should eq(WN)
    mv.promoted_piece.should eq(EMPTY)
    fail "flag error" if 0 == (mv.value & Move::MFLAGCA)
  end

  example "#capture? detects capture moves" do
    mv = Move.assemble(A2, B3)
    fail "flag error" if mv.capture?
    mv = Move.assemble(A2, B3, WP)
    fail "flag error" unless mv.capture?
    mv = Move.assemble(A2, B3, BQ)
    fail "flag error" unless mv.capture?
  end

  example "#promotion? detects promotion moves" do
    mv = Move.assemble(A2, B3)
    fail "flag error" if mv.promotion?
    mv = Move.assemble(A2, B3, EMPTY, WP)
    fail "flag error" unless mv.promotion?
    mv = Move.assemble(A2, B3, EMPTY, BQ)
    fail "flag error" unless mv.promotion?
  end

  example "#pawn_start? detects pawn starts" do
    mv = Move.assemble(A2, B3)
    fail "flag error" if mv.pawn_start?
    mv = Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS)
    fail "flag error" unless mv.pawn_start?
  end

  describe "#to_s" do
    example "a quiet move" do
      Move.assemble(A2, B3).to_s.should eq("a2b3")
    end

    example "a pawn start move" do
      Move.assemble(A2, A4, EMPTY, EMPTY, Move::MFLAGPS).to_s.should eq("a2a4/ps")
    end

    example "a capture move" do
      Move.assemble(A2, B3, BQ).to_s.should eq("a2b3/cap:q")
    end

    example "a promotion move" do
      Move.assemble(A2, B3, EMPTY, BQ).to_s.should eq("a2b3/->q")
    end

    example "a castling move" do
      Move.assemble(E1, G1, EMPTY, EMPTY, Move::MFLAGCA).to_s.should eq("e1g1/cas")
    end
  end

  example "#to_s_compact" do
    Move.assemble(A2, B3           ).to_s_compact.should eq("a2b3" )
    Move.assemble(A7, A8, EMPTY, WQ).to_s_compact.should eq("a7a8Q")
    Move::NOMOVE.to_s_compact.should eq("NOMOVE")
  end
end
