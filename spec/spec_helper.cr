$test_mode = true
$test_pv_table_size = (2**12).to_i
# use a smaller table to speed up the test.
# Warning: the smaller the table, the higher the risk of collisions.
#

require "spec"
require "../src/chess"

module Spec::DSL
  def example(*args, &block)
    it(*args, &block)
  end
end

Spec.before_each do
  Chess.reset_before_test
end

# PIECE_VAL = { 0,
# 100,   325,   325,   550,   1000,  50_000,
def create_board_1_and_update_lists_materials
  Chess::Board.new.tap do |b|
    b.set_piece A1,  WR   #    550 - big, major
    b.set_piece B1,  WN   #    325 - big, min
    b.set_piece C1,  WB   #    325 - big, min
    b.set_piece D1,  WQ   #  1_000 - big, major
    b.set_piece E1,  WK   # 50_000 - big, major
    b.set_piece F1,  WB   #    325 - big, min
    b.set_piece G1,  WN   #    325 - big, min
    b.set_piece H1,  WR   #    550 - big, major

    b.set_piece A2,  WP   #    100 - bit 8
    b.set_piece B2,  WP   #    100 - bit 9
    b.set_piece C2,  WP   #    100 - bit 10
    b.set_piece D2,  WP   #    100 - bit 11
    b.set_piece E2,  WP   #    100 - bit 12
    b.set_piece F2,  WP   #    100 - bit 13
    b.set_piece G2,  WP   #    100 - bit 14
    b.set_piece H2,  WP   #    100 - bit 15

    b.set_piece A7, BP   #    100 - bit 48
    b.update_lists_materials
  end
end

NO_SQS = [NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ, NO_SQ]
def generate_moves(fen, side)
  board = Board.parse_fen(fen)
  moves = MoveGenerator.new(board, side).generate
end

def white_moves(fen)
  generate_moves(fen, WHITE)
end

def black_moves(fen)
  generate_moves(fen, BLACK)
end

def generate_moves_a(ascii, side)
  board = Board.parse_ascii(ascii)
  # Chess::Debug.print_board board
  moves = MoveGenerator.new(board, side).generate
end

def white_moves_a(fen)
  generate_moves_a(fen, WHITE)
end

def black_moves_a(fen)
  generate_moves_a(fen, BLACK)
end


BEF_FEN_1  =   "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_MOV_B8_A6 =   "r1bqkbnr/pppppppp/n7/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_MOV_A7_A6 =   "rnbqkbnr/1ppppppp/p7/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_FEN_A7 =   "rnbqkbnr/1ppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_FEN_A8 =   "1nbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_FEN_B8 =   "r1bqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_FEN_D7 =   "rnbqkbnr/ppp1pppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
AFT_FEN_H8 =   "rnbqkbn1/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

KIWIPETE   = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1"

def cell_to_s(board, sq120, msg="")
  msg + Board::Algebraic.algebraic_for(sq120) + " #{Piece.char_for_piece(board.piece(sq120))}"
end
