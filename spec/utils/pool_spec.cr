require "./../spec_helper"

class StringList < Array(String)
end

describe Utils::Pool do
  example "a virgin pool" do
    pool = Utils::Pool(StringList).new
    pool.used    .should eq(0)
  end

  example "getting the first element" do
    pool = Utils::Pool(StringList).new
    pool.get_one.tap do |sl|
      sl         .should be_a(StringList)
      sl.size    .should eq(0)
    end
    pool.used    .should eq(1)
  end

  example "getting two elements in sequence" do
    pool = Utils::Pool(StringList).new
    sl1   = pool.get_one
    sl2   = pool.get_one

    pool.used    .should eq(2)

    sl1.object_id.should_not eq(sl2.object_id)
  end

  describe "#give_back" do
    it "recycles unused elements" do
      pool = Utils::Pool(StringList).new
      sl1, sl2, sl3  = pool.get_one, pool.get_one, pool.get_one

      pool.give_back(sl2)         # sl2 is now "unused"
      pool.used    .should eq(2)

      (pool.get_one).object_id.should eq(sl2.object_id) # sl2 is recycled
      pool.used    .should eq(3)
    end

    it "clears the contents of recycled elements" do
      pool = Utils::Pool(StringList).new
      sl1 = pool.get_one.tap do |sl|
        sl.push("un").push("deux")
        sl.to_a.should eq(["un", "deux"])
      end

      pool.give_back(sl1)

      pool.get_one.tap do |recycled_sl|
        recycled_sl.object_id.should eq(sl1.object_id)
        recycled_sl.to_a.should eq([] of String)
      end
    end
  end

# TODO :
# - #reset

end
