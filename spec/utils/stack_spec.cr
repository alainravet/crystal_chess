require "./../spec_helper"

describe Utils::Stack do
  example "normal usage" do
    st = Utils::Stack(String).new()
    st.size.should eq(0)
    st.push("un").push("deux")
    st.size.should eq(2)
    st.top.should eq("deux")
    st.size.should eq(2)
    st.pop.should eq("deux")
    st.size.should eq(1)
    st.pop.should eq("un")
    st.size.should eq(0)
  end

  example "#push raises a CapacityError if the stack is full" do
    st = Utils::Stack(String).new(1)
    st.push "ok"
    expect_raises(Utils::Stack::CapacityError) do
      st.push "fail"
    end
  end

  example "#top returns nil when the stack is empty" do
    st = Utils::Stack(String).new()
    st.top.should be_nil
  end

  example "#pop raises a Stack::EmptyStack when the stack is empty" do
    st = Utils::Stack(String).new()
    expect_raises(Utils::Stack::EmptyStack) do
     st.pop
    end
  end

  describe "#values" do
    it "returns [] when the stack is empty" do
      Utils::Stack(String).new().values.should eq([] of String)
    end

    it "returns the stack contents in FIFO" do
      st = Utils::Stack(String).new()
      st.push("un").push("deux")
      st.values.should eq({"un", "deux"})
    end
  end
end
